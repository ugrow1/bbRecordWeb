import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { route } from "../Hooks";
import { Link } from "react-router-dom";

export const SignIn = async (email, password) => {
  //Method that resends the confirmation email if the user hasn't been confirmed
  const sendEmailConfirmation = () => {
    try {
      axios.post(route + "/auth/send-email-confirmation", {
        email,
      });
    } catch (error) {
      console.log(error);
    }
  };

  //Try to login
  try {
    const response = await axios.post(route + "/auth/local", {
      identifier: email,
      password: password,
    });
    const user = response.data;
    return { success: { authUser: user }, error: undefined };
  } catch (error) {
    console.log(
      "🚀 ~ file: LoginForm.js ~ line 24 ~ SignIn ~ error",
      error.response.data?.message[0]?.messages[0]?.message
    );
    if (
      error.response.data?.message[0]?.messages[0]?.message ===
      "Your account email is not confirmed"
    ) {
      sendEmailConfirmation();
    }

    return { success: undefined, error: error };
  }
};

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      raiseAlert: { status: false, message: "" },
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const trySignIn = await SignIn(this.state.email, this.state.password);

    //IF AN ERROR OCURRED, WE SWITCH ON THE ERROR TO SHOW THE CORRECT ERROR MESSAGE
    if (trySignIn.error) {
      switch (trySignIn.error.response.data?.message[0]?.messages[0]?.message) {
        case "Your account email is not confirmed":
          this.setState({
            raiseAlert: {
              status: true,
              message: "Por favor confirme su correo electrónico",
            },
          });

          break;

        case "Identifier or password invalid.":
          this.setState({
            raiseAlert: {
              status: true,
              message: "Usuario o contraseña incorrectos",
            },
          });
          break;

        default:
          this.setState({
            raiseAlert: {
              status: true,
              message:
                "Error desconocido: " +
                trySignIn.error.response.data?.message[0]?.messages[0]?.message,
            },
          });

          break;
      }
      return;
    }

    if (trySignIn.success.authUser.user.role.name !== "Doctor") {
      this.setState({
        raiseAlert: {
          status: true,
          message: "Usuario o contraseña incorrecta",
        },
      });
      return;
    }

    this.props.setUser(trySignIn.success.authUser);

    //sets state variable with the promise of the endpoint call
  }

  render() {
    return (
      <>
        {this.state.raiseAlert.status && (
          <div className="alert alert-danger" role="alert">
            {this.state.raiseAlert.message}
          </div>
        )}
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Correo electrónico</label>
            <input
              name="email"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
              id="userEmail"
            />
          </div>
          <div className="form-group">
            <label>Contraseña</label>
            <input
              type="password"
              name="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChange}
              id="exampleInputPassword1"
            />
          </div>
          <div className="nav-item">
            <a className="nav-link" href="/forgotpassword">
              ¿Olvidaste la contraseña?
            </a>
          </div>
          <ul className="nav nav-pills m-0 mt-4 card-header-pills">
            <li className="nav-item">
              <input
                type="submit"
                className="btn btn-dark rounded-pill px-4"
                value="Entrar"
              />
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/registration">
                Registrar
              </Link>
            </li>
          </ul>
        </form>
      </>
    );
  }
}
LoginForm.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default LoginForm;
