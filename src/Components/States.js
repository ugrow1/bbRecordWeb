import React, { useState, useEffect } from "react";

export default function States({ state, possibleStates }) {
  // const [actualState, setActualState] = useState(state);

  // useEffect(() => {
  //   console.log(actualState, state);
  //   if (actualState !== state) {
  //     console.log("adentro");
  //     setActualState(state);
  //   }
  // }, [actualState]);

  // const handleClick = (e) => {
  //   setActualState(e.target.id);
  // };

  // To determine the badge thas needs to be rendered, we read the current state of the entry and search on the possibleStates variable to get the value of the badge
  // the badge text will be the state of the entry with the "_" replaced to " "
  const stateBadge = (
    <span className={"px-3 py-2 badge badge-" + possibleStates[state]}>
      {state.replace("_", " ")}
    </span>
  );

  return (
    <div className="dropdown">
      <button
        className="btn btn-sm border-0 p-0 btn-outline-light"
        type="button"
        id="dropdownMenuButton"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <h6>{stateBadge}</h6>
      </button>
      {/* <div className="dropdown-menu p-0" aria-labelledby="dropdownMenuButton">
          <a className="dropdown-item px-3 small" id="En_proceso" onClick ={this.handleClick.bind(this)}>En proceso</a>
          <a className="dropdown-item px-3 small" id="Abierta" onClick ={this.handleClick.bind(this)}>Abierta</a>
          <a className="dropdown-item px-3 small" id="Para_facturar"onClick ={this.handleClick.bind(this)}>Para facturar</a>
          <a className="dropdown-item px-3 small" id="Finalizada"onClick ={this.handleClick.bind(this)}>Finalizada</a>
          <a className="dropdown-item px-3 small" id="Cancelada" onClick ={this.handleClick.bind(this)}>Cancelada</a>
        </div> */}
    </div>
  );
}
