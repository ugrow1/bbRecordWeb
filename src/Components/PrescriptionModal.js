import axios from "axios";
import React from "react";
import { AutoComplete, TextInput, SelectRelations } from "../Forms/Inputs";
import { route } from "../Hooks";
import moment from "moment";

class PrescriptionModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prescripted: [{ medicine: "", dose: "" }],
      medicines: [],
      baby: "",
      clinic: "",
      dose: "",
    };
  }

  async componentDidMount() {
    const authUser = JSON.parse(sessionStorage.getItem("authUser"));

    try {
      const medicineResponse = await axios(route + "/medicines", {
        headers: { Authorization: `Bearer ${authUser.jwt}` },
      });

      const medicinesNames = medicineResponse.data.map(
        (medicine) => medicine.name
      );

      this.setState({
        authUser: authUser,
        medicines: medicinesNames,
        redirect: false,
      });

      const prescriptionResponse = await axios(
        route + "/prescriptions/" + this.props.prescriptionId,
        { headers: { Authorization: `Bearer ${authUser.jwt}` } }
      );

      const data = prescriptionResponse.data;

      this.setState({
        prescripted: data.message,
        baby: this.props.patientId,
        clinic: data.clinic.id,
        image: data.image[data.image.length - 1]?.url,
      });
    } catch (error) {}
  }
  //ITERATE EACH FIELD TO CHECK IF ARE FILLED

  render() {
    const authUser = JSON.parse(sessionStorage.getItem("authUser"));

    if (this.state.redirect) {
      window.location.href = "/prescriptions";
    }

    const {
      handleChange,

      state: { clinic, medicines, prescripted, baby },
    } = this;

    var medicineRows = prescripted.map((medicine, index) => (
      <div className="my-5" key={index}>
        <div className="form-row ">
          <div className="form-group col-md-10">
            <AutoComplete
              field="Medicamento"
              name={index.toString()}
              value={prescripted[index].medicine}
              disabled
            />
          </div>
        </div>
        <TextInput
          field="Dosis"
          text={prescripted[index].dose}
          name={index.toString()}
          disabled
        />
      </div>
    ));

    if (authUser) {
      return (
        <div
          className="modal fade "
          id={
            this.props.prescriptionId
              ? "viewPrescription" + this.props.patientId
              : "viewPrescription" + this.props.prescriptionId
          }
          data-backdrop="false"
          data-keyboard="false"
          tabIndex="1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog   modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="staticBackdropLabel">
                  Ver Prescripcion
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <SelectRelations
                        name="baby"
                        defaultValue={baby && baby}
                        query={"?doctors_in=" + authUser.user.id}
                        collection="babies"
                        attributes={["name"]}
                        disabled
                      />
                      <hr></hr>
                    </div>
                  </div>
                  <div className="col-8">
                    <div className="card">
                      <div className="card-body">
                        <SelectRelations
                          field="Clínica"
                          name="clinic"
                          query={"?doctors_in=" + authUser.user.id}
                          defaultValue={clinic}
                          collection="clinics"
                          attributes={["name"]}
                          disabled
                        />
                        {medicineRows}
                        {/* SHOWS PREVIOUS IMAGE WHEN EDITING */}

                        <img
                          src={route + this.state.image}
                          alt={this.state.title}
                          className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Cerrar
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return null;
  }
}

export default PrescriptionModal;
