import { SendNotificationModal, MedicineModal, PrescriptionModal } from ".";
import { useLocation, Link } from "react-router-dom";
export default function Actions({ correspondingEntry, actions, babyId }) {
  const location = useLocation();

  const actionButtons = actions.map((action) => (
    <Link
      type="button"
      className=" btn btn-sm btn-outline-secondary "
      title={action.displayText}
      data-target={
        action.target
          ? action.target + (babyId ? babyId : correspondingEntry)
          : undefined
      }
      data-toggle={action.target ? "modal" : undefined}
      to={action.link ? action.link + correspondingEntry : undefined}
    >
      <span className="material-icons">{action.icon}</span>
    </Link>
  ));

  return (
    <div id="actions">
      {actionButtons}

      {location.pathname === "/medicines" && (
        <MedicineModal medicineId={correspondingEntry} />
      )}
      {console.log(babyId)}
      {(location.pathname === "/appointments" ||
        location.pathname === "/menu") && (
        <SendNotificationModal patientId={babyId} />
      )}
      {location.pathname === "/inbox" && (
        <SendNotificationModal patientId={correspondingEntry} />
      )}
      {location.pathname === "/prescriptions" && (
        <PrescriptionModal
          prescriptionId={correspondingEntry}
          patientId={babyId}
        />
      )}
    </div>
  );
}
