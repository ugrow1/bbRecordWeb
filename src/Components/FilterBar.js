import React from "react";

class FilterBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
  this.handleChange = this.handleChange.bind(this);
  this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }
  onSubmit(e) {
    e.preventDefault();
    var title = this.title;
    this.props.value(title.value)
    
}

 render() {
  
  return (
    <div className="px-3 py-0 mb-3">
      <div className="form-row">
        <div className="form-group col">
          <form action="">
            <label className="text-muted small text-uppercase">
              Buscar {this.props.buscador}
            </label>
            <div className="input-group">
              <input
                className="form-control form-control"
                type="text"
                placeholder="Buscar"
                value={this.state.value} 
                onChange = {this.handleChange}
                ref={(c) => this.title = c} name="title"
              />
              <button
                type="submit"
                className="form-control col-3 col-md-2 border btn btn-light"
                onClick={this.onSubmit}
              >
                Buscar
              </button>
            </div>
          </form>
        </div>

        {/* <div className="form-group col-lg-auto">
          <form action="">
            <label className="text-muted small text-uppercase">
              Filtra por fecha
            </label>
            <div className="input-group">
              <input
                className="form-control form-control"
                type="text"
                placeholder="Fecha desde"
              />
              <input
                className="form-control form-control"
                type="text"
                placeholder="Fecha hasta"
              />
              <button
                type="submit"
                className="form-control col-3 col-md-2 border btn btn-light"
              >
                Buscar
              </button>
            </div>
          </form>
        </div> */}
      </div>
    </div>
  );
}
}
export default FilterBar;
