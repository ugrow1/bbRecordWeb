import { useEffect, useState } from "react";
import axios from "axios";
import { route, useFormControl } from "../Hooks";
import { AuthUser } from "../Authentication";

export default function MedicineModal({ medicineId }) {
  const { areFieldsEmpty } = useFormControl();
  const { token } = AuthUser();

  const [state, setState] = useState({
    name: "",
    indications: "",
    restrictions: "",
    price: "",
  });

  const [submitStatus, setSubmitStatus] = useState({});

  useEffect(() => {
    async function getData() {
      const response = await axios.get(route + "/medicines/" + medicineId, {
        headers: { Authorization: `Bearer ${token}` },
      });

      setState({
        name: response.data.name,
        indications: response.data.indications,
        restrictions: response.data.restrictions,
        price: response.data.price,
      });
    }
    if (medicineId) {
      getData();
    }
  },[] );

  async function onSubmit() {
    if (areFieldsEmpty(state)) {
      setSubmitStatus({ invalidFields: true });
      return;
    }

    var method = axios.post;
    var requestRoute = route + "/medicines";
    if (medicineId) {
      method = axios.put;
       requestRoute = route + "/medicines/" + medicineId;
    }
    try {
      await method(requestRoute, state, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setSubmitStatus({ notificationSent: true });
    } catch (error) {
      setSubmitStatus({ notificationNotSent: true });
      console.error(error.response);
    }
  }

  function handleChange(e) {
    const stateName = e.target.name;
    setState((prevState) => ({ ...prevState, [stateName]: e.target.value }));
  }
  const { name, indications, restrictions, price } = state;
  return (
    <div
      className="modal fade "
      id={medicineId ? "medicineData" + medicineId : "medicineData"}
      data-backdrop="false"
      data-keyboard="false"
      tabIndex="1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog   modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Medicamento
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {/* MEDICINE NAME */}
            <div className="input-group mb-3">
              <input
                type="text"
                onChange={handleChange}
                className="form-control"
                name="name"
                value={name}
                placeholder="Nombre"
                aria-label="Nombre"
                aria-describedby="basic-addon1"
              />
            </div>
            {/* MEDICINE PRICE */}
            <div className="input-group mb-3">
              <input
                type="number"
                onChange={handleChange}
                className="form-control"
                name="price"
                value={price}
                placeholder="Precio"
                aria-label="Precio"
                aria-describedby="basic-addon1"
              />
            </div>
            {/* MEDICINE INDICATIONS */}
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">Indicaciones</span>
              </div>
              <textarea
                name="indications"
                onChange={handleChange}
                value={indications}
                className="form-control"
                aria-label="Mensaje"
              ></textarea>
            </div>
            {/* MEDICINE RESTRICTIONS */}
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <span className="input-group-text">Restricciones</span>
              </div>
              <textarea
                name="restrictions"
                onChange={handleChange}
                value={restrictions}
                className="form-control"
                aria-label="Mensaje"
              ></textarea>
            </div>
          </div>
          {/* Invalid fields message */}
          {submitStatus.invalidFields && (
            <div className="alert alert-danger" role="alert">
              Por favor llene todos los campos correctamente
            </div>
          )}
          {/* Notification sent message */}
          {submitStatus.notificationSent && (
            <div className="alert alert-success" role="alert">
              ¡Medicamento guardado!
            </div>
          )}
          {submitStatus.notificationNotSent && (
            <div className="alert alert-danger" role="alert">
              Ha ocurrido un error guardando los cambios
            </div>
          )}

          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Cerrar
            </button>
            <button
              onClick={onSubmit}
              type="button"
              className="btn btn-primary"
            >
              Guardar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
