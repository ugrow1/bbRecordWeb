import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { Actions, States } from ".";
import moment from "moment";
import { useLocation } from "react-router";

//get the requested database fields written on props.fields

function DataRow({ fields, data }) {
  return fields.map((field) => {
    // FIELD VALUE IS FILLED WITH THE VALUE GIVEN BY THE PROPPERTY AFTER BEING RUNNED THROUGH THAT FUNCTION
    var fieldValue = field.name.split(".").reduce((prop, nestedProp) => {
      return prop[nestedProp];
    }, data);

    switch (field.type) {
      case "date":
        return (
          <td key={fieldValue + "date"}>
            {moment(fieldValue).format("YYYY-MM-DD")}
          </td>
        );
      case "time":
        return (
          <td key={fieldValue + "time"}>
            {moment(fieldValue).subtract(4, "h").format("h:mm A")}
          </td>
        );

      case "dateTime":
        return [
          <td key={fieldValue + "date"}>
            {moment(fieldValue).format("YYYY-MM-DD")}
          </td>,
          <td key={fieldValue + "time"}>
            {moment(fieldValue).subtract(4, "h").format("h:mm A")}
          </td>,
        ];
      case "price":
        return (
          <td key={fieldValue + "price"} style={{ color: "green" }}>
            RD $ {fieldValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
          </td>
        );

      default:
        return <td key={data.id + "." + field.name}> {fieldValue} </td>;
    }
  });
}

export default function TableData({
  endpoint,
  setEntriesAmount,
  limitLeft,
  limitRight,
  fields,
  states,
  actions,
}) {
  const [databaseData, setDatabaseData] = useState([]);
  const prevEndpoint = useRef();
  const location = useLocation();

  //executes when component is mounted on the web browser
  useEffect(() => {
    async function getData() {
      //LOGGED USER DATA
      const userString = sessionStorage.getItem("authUser");
      const user = JSON.parse(userString);
      try {
        //GETS ENDPOINT DATA GIVEN BY THE PROPS
        const response = await axios.get(endpoint, {
          headers: { Authorization: `Bearer ${user.jwt}` },
        });

        //IF THE COMPONENT IF SPECIFIED TO HAVE PAGINATION, THEN SETS THE AMOUNT OF PAGES BASED ON THE AMOUNT OF ENTRIES RETURNED ON THE REQUEST
        if (setEntriesAmount) {
          setEntriesAmount(response.data.length);
        }

        setDatabaseData(response.data);
      } catch (error) {
        console.log(error);
      }
    }
    if (prevEndpoint.current !== endpoint) {
      getData();
    }

    prevEndpoint.current = endpoint;
  }, [endpoint]);

  return databaseData.slice(limitLeft, limitRight).map((data, index) => (
    <tr key={index}>
      <th scope="row">{data.id}</th>
      <DataRow fields={fields} data={data} />

      {/* if states is true, then this will generate states field */}
      {states && (
        <td>
          <States state={data.state} possibleStates={states} />
        </td>
      )}

      {/* if actions has been set, then this will generate actions field */}
      {actions && (
        <td>
          <Actions
            correspondingEntry={data.id}
            babyId={data.baby?.id}
            actions={actions}
          />
        </td>
      )}
    </tr>
  ));
}
