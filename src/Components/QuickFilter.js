function QuickFIlter(props) {
  const fields = props.filters.map((filter) => (
    <li key={filter} className="nav-item">
      <a
        onClick={() => props.displayFiltered(filter.replaceAll(" ", "_"))}
        className="nav-link"
      >
        {filter}
      </a>
    </li>
  ));

  return (
    <div className="card-header bg-white px-0 py-0">
      <ul className="nav nav-tabs card-header-tabs m-0">{fields}</ul>
    </div>
  );
}

export default QuickFIlter;
