import axios from "axios";
import React from "react";
import { route } from "../../Hooks";

export default class ForgotPasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { password: "", passwordConfirmation: "" };
    this.handleChange = this.handleChange.bind(this);
    this.sendRecoveryMail = this.sendRecoveryMail.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  async sendRecoveryMail(event) {
    event.preventDefault();
    this.setState({ raiseAlert: "" });
    try {
      await axios.post(route + "/auth/forgot-password", {
        email: this.state.email,
      });
      this.setState({ raiseAlert: "succeed" });
    } catch (error) {
      console.log(error.response.data);
      if (
        error.response?.data?.message[0]?.messages[0]?.message ===
        "This email does not exist."
      ) {
        this.setState({ raiseAlert: "invalidEmail" });
        return;
      }
      this.setState({ raiseAlert: "unknownError" });
    }
  }

  render() {
    return (
      <>
        <form onSubmit={this.sendRecoveryMail}>
          <div className="form-group">
            <label>Correo electrónico</label>
            <input
              type="email"
              name="email"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>

          <div
            className={`alert alert-${
              this.state.raiseAlert
                ? alertMessages[this.state.raiseAlert].type
                : "d-none"
            }`}
            role="alert"
          >
            {alertMessages[this.state.raiseAlert]?.message}
          </div>

          <ul className="nav nav-pills m-0 mt-4 card-header-pills">
            <li className="nav-item">
              <input
                type="submit"
                className="btn btn-dark rounded-pill px-4"
                value="Cambiar contraseña"
              />
            </li>
          </ul>
        </form>
      </>
    );
  }
}

const alertMessages = {
  invalidEmail: {
    type: "danger",
    message: "Este correo no está registrado",
  },
  succeed: {
    type: "success",
    message:
      "Se ha enviado un correo a la dirección especificada con las instrucciones a seguir",
  },
  unknownError: {
    type: "danger",
    message: "Ha ocurrido un error. inténtelo más tarde",
  },
};
