import axios from "axios";
import React from "react";
import { Redirect } from "react-router-dom";
import { route, currentOs } from "../../Hooks";

export default class ResetPasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { password: "", passwordConfirmation: "", redirect: false };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRedirection = this.handleRedirection.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleRedirection() {
    console.log("in");
    if (currentOs === "Android") {
      //THE HREF WILL CHANGE TO THE APP SCHEEME 'infanty://'
      window.location.href = `exp://192.168.0.10:19000/--/SignIn`;
    } else {
      this.setState({ redirect: true });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();

    if (this.state.password !== this.state.passwordConfirmation) {
      this.setState({ raiseAlert: "passwordsNotEqual" });
      return;
    }
    try {
      const response = await axios.post(route + "/auth/reset-password", {
        code: this.props.code,
        password: this.state.password,
        passwordConfirmation: this.state.passwordConfirmation,
      });
      console.log(response.data);
      this.setState({ raiseAlert: "succeed" });
      setTimeout(this.handleRedirection, 1500);
    } catch (error) {
      alert(error);
      if (
        error.response?.data?.message[0]?.messages[0]?.message ===
        "Incorrect code provided."
      ) {
        this.setState({ raiseAlert: "invalidCode" });
        return;
      }
      this.setState({ raiseAlert: "unknownError" });
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to={`/login`} />;
    }
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Nueva contraseña</label>
            <input
              type="password"
              name="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Confirmar contraseña</label>
            <input
              type="password"
              name="passwordConfirmation"
              className="form-control"
              value={this.state.passwordConfirmation}
              onChange={this.handleChange}
            />
          </div>
          <div
            className={`alert alert-${
              this.state.raiseAlert
                ? alertMessages[this.state.raiseAlert].type
                : "d-none"
            }`}
            role="alert"
          >
            {alertMessages[this.state.raiseAlert]?.message}
          </div>

          <ul className="nav nav-pills m-0 mt-4 card-header-pills">
            <li className="nav-item">
              <input
                type="submit"
                className="btn btn-dark rounded-pill px-4"
                value="Cambiar contraseña"
              />
            </li>
          </ul>
        </form>
      </>
    );
  }
}
const alertMessages = {
  passwordsNotEqual: {
    type: "danger",
    message: "Las contraseñas no son iguales",
  },
  succeed: {
    type: "success",
    message: "Se ha cambiado su contraseña con éxito",
  },
  invalidCode: {
    type: "danger",
    message: "Solicitud expirada",
  },
  unknownError: {
    type: "danger",
    message: "Ha ocurrido un error. inténtelo más tarde",
  },
};
