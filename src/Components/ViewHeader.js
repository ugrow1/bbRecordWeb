import { Link, useLocation } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { route } from "../Hooks";
import axios from "axios";

export default function ViewHeader({
  displayModal = false,
  HeaderText,
  Navigate,
  onClick,
  ViewAction,
  modalTarget,
  modalToDisplay,
}) {
  const location = useLocation();
  const userString = sessionStorage.getItem("authUser");
  var authUser = JSON.parse(userString);
  const [babies, setbabies] = useState();

  useEffect(() => {
    const userString = sessionStorage.getItem("authUser");
    var authUser = JSON.parse(userString);
    async function getBabies() {
      const babiesInDoctor = await axios.get(
        route + "/babies?doctors_in=" + authUser.user.id,
        {
          headers: { Authorization: `Bearer ${authUser.jwt}` },
        }
      );
      setbabies(babiesInDoctor.data);
      sessionStorage.setItem("babiesAmount", babiesInDoctor.data.length);
    }
    getBabies();
  }, []);
  return (
    <div className="container-fluid mb-3">
      <div className="row justify-content-between">
        <div className="col-auto">
          <h2 className="h4 mb-3">{HeaderText}</h2>
        </div>
        <div className="col-auto">
          <ul className="nav">
            <li className="nav-item ml-2">
              {location.pathname.includes("/appointments") &&
              (authUser.user.clinics.length === 0 || babies?.length === 0) ? (
                <div className="alert alert-danger" role="alert">
                  Debe agregar bebe y clinica antes de crear una cita
                </div>
              ) : displayModal ? (
                <>
                  <button
                    type="button"
                    data-toggle="modal"
                    data-target={modalTarget}
                    className="btn btn-success"
                  >
                    {ViewAction}
                  </button>
                  {modalToDisplay}
                </>
              ) : (
                <Link
                  to={Navigate}
                  type="button"
                  onClick={onClick}
                  className="btn btn-success"
                >
                  {ViewAction}
                </Link>
              )}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
