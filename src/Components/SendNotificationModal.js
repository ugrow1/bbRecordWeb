import { useEffect, useState } from "react";
import axios from "axios";
import { route, useFormControl } from "../Hooks";
import { AuthUser } from "../Authentication";

export default function SendNotificationModal({ patientId, id }) {
  const { areFieldsEmpty } = useFormControl();
  const { authUser, token } = AuthUser();

  const [patients, setPatients] = useState();
  const [state, setState] = useState({
    message: "",
    report_type: "",
    creator: authUser.id,
    doctor: authUser.id,
    baby: patientId ? patientId : "",
    title: "",
    date: new Date(),
  });

  const [submitStatus, setSubmitStatus] = useState({});

  useEffect(() => {
    async function getData() {
      if (patientId === undefined) {
        const patientsRequest = await axios.get(
          route + "/babies?doctors_in=" + authUser.id
        );
        setPatients(patientsRequest.data);
      } else {
        const patientRequest = await axios.get(
          route + "/babies?id=" + patientId,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );

        setPatients(patientRequest.data);
      }
      await axios.get(route + "/report-types");
    }

    getData();
  }, [patientId]);

  async function onSubmit() {
    if (areFieldsEmpty(state)) {
      setSubmitStatus({ invalidFields: true });
      return;
    }

    try {
      await axios.post(route + "/reports", state, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      setSubmitStatus({ notificationSent: true });
    } catch (error) {
      setSubmitStatus({ notificationNotSent: true });
      console.error(error.response);
    }
  }

  function handleChange(e) {
    const stateName = e.target.name;
    setState((prevState) => ({ ...prevState, [stateName]: e.target.value }));
  }
  return (
    <div
      className="modal fade "
      id={patientId ? "sendNotification" + patientId : "sendNotification"}
      data-backdrop="false"
      data-keyboard="false"
      tabIndex="1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog   modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Enviar Mensaje
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {/* Select patient to send the notification */}
            <div className="input-group mb-3">
              {patients && (
                <select
                  className="custom-select"
                  id="inputGroupSelect04"
                  onChange={handleChange}
                  name="baby"
                  defaultValue={patientId && patientId}
                  disabled={patientId ? true : false}
                  aria-label="Example select with button addon"
                >
                  <option>Nombre del paciente: </option>
                  {patients.map((patient) => (
                    <option key={patient.id} value={patient.id}>
                      {patient.name} {patient.last_name}
                    </option>
                  ))}
                </select>
              )}
            </div>
            {/* Select the type of the nortification */}
            <div className="input-group mb-3">
              <select
                className="custom-select"
                id="inputGroupSelect04"
                onChange={handleChange}
                name="report_type"
                aria-label="Example select with button addon"
              >
                <option>Tipo de notificación: </option>

                <option value={2}>Alerta</option>
                <option value={3}>Mensaje</option>
              </select>
            </div>
            {/* Notification title */}
            <div className="input-group mb-3">
              <input
                type="text"
                onChange={handleChange}
                className="form-control"
                name="title"
                placeholder="Título"
                aria-label="Título"
                aria-describedby="basic-addon1"
              />
            </div>
            {/* Message of the notification */}
            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Descripción</span>
              </div>
              <textarea
                name="message"
                onChange={handleChange}
                className="form-control"
                aria-label="Mensaje"
              ></textarea>
            </div>
          </div>
          {/* Invalid fields message */}
          {submitStatus.invalidFields && (
            <div className="alert alert-danger" role="alert">
              Por favor llene todos los campos correctamente
            </div>
          )}
          {/* Notification sent message */}
          {submitStatus.notificationSent && (
            <div className="alert alert-success" role="alert">
              ¡Mensaje enviado!
            </div>
          )}
          {submitStatus.notificationNotSent && (
            <div className="alert alert-danger" role="alert">
              Ha ocurrido un error enviando su mensaje
            </div>
          )}

          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Cerrar
            </button>
            <button
              onClick={onSubmit}
              type="button"
              className="btn btn-primary"
            >
              Enviar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
