import React from "react";
import axios from "axios";
import { Table } from "react-bootstrap";
import { route } from "../Hooks";

class Statistics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointmentsLength: "",
      babiesLength: "",
      priceTotal: "",
      today: this.props.today,
      nextDate: this.props.nextDate,
    };
  }
  async componentDidMount() {
    const user = JSON.parse(sessionStorage.getItem("authUser"));
    this.setState({ authUser: user });
    const infoAppointments = await axios.get(
      route +
        "/appointments?doctor=" +
        user.user.id +
        "&date_gte=" +
        this.props.today +
        "&date_lte=" +
        this.props.nextDate +
        "&state=Finalizada",
      { headers: { Authorization: `Bearer ${user.jwt}` } }
    );
    const infoBabies = await axios.get(
      route + "/babies?doctors=" + user.user.id,
      { headers: { Authorization: `Bearer ${user.jwt}` } }
    );
    const result = Object.values(infoAppointments.data).reduce(
      (r, { price }) => r + price,
      0
    );
    this.setState({
      appointmentsLength: infoAppointments.data,
      babiesLength: infoBabies.data,
      priceTotal: result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
    });
  }

  componentDidUpdate() {
    if (
      this.state.nextDate !== this.props.nextDate ||
      this.state.today !== this.props.today
    ) {
      this.setState({ today: this.props.today, nextDate: this.props.nextDate });
      this.componentDidMount();
    }
  }

  render() {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Total de ingresos</th>
            <th>Total de citas</th>
            <th>Pacientes registrados</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>RD ${this.state.priceTotal}</td>
            <td>{this.state.appointmentsLength.length}</td>
            <td>{this.state.babiesLength.length}</td>
          </tr>
        </tbody>
      </Table>
    );
  }
}

export default Statistics;
