import moment from "moment";
import { useState } from "react";
import { Redirect } from "react-router-dom";

export default function JsonDataModal({ data, babyId }) {
  const [redirect, setRedirect] = useState();
  if (data) {
    if (redirect) {
      return (
        <Redirect
          push
          to={`/editbabyfield/${babyId}/${data.view}/${JSON.stringify(
            data.info
          )}`}
        />
      );
    }

    return (
      <div
        className="modal fade"
        id="displayData"
        data-backdrop="false"
        data-keyboard="false"
        tabIndex="1"
        aria-labelledby="staticBackdropLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="staticBackdropLabel">
                {data.label}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body  ">
              <div class="card user-card-full">
                <div class="row m-l-0 m-r-0">
                  <div class="col-sm-8">
                    <div class="card-block">
                      <div class="row">
                        {data.columns.map((columnName) => (
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">{columnName}</p>
                          </div>
                        ))}
                      </div>

                      {data.info.map((entry) => (
                        <div class="row">
                          {data.view === "suffered_diseases" ? (
                            <>
                              <div class="col-sm-6">
                                <h6 class="text-muted f-w-400">
                                  {entry.disease.name}
                                </h6>
                              </div>
                              <div class="col-sm-6">
                                <h6 class="text-muted f-w-400">
                                  {moment(entry.started_at).format(
                                    "DD/MM/yyyy"
                                  )}
                                </h6>
                              </div>
                              <div class="col-sm-6">
                                <h6 class="text-muted f-w-400">
                                  {moment(entry.finished_at).format(
                                    "DD/MM/yyyy"
                                  )}
                                </h6>
                              </div>
                            </>
                          ) : (
                            data.attributes.map((attribute) => (
                              <div class="col-sm-6">
                                {data.view === "injected_vaccines" ? (
                                  <h6 class="text-muted f-w-400">
                                    {entry.vaccine.name}
                                  </h6>
                                ) : (
                                  <h6 class="text-muted f-w-400">
                                    {entry[attribute]
                                      ? moment(
                                          entry[attribute],
                                          moment.ISO_8601,
                                          true
                                        ).isValid()
                                        ? moment(entry[attribute]).format(
                                            "DD/MM/yyyy"
                                          )
                                        : `${entry[attribute]} ${
                                            data.sufix ? data.sufix : ""
                                          }`
                                      : ""}
                                  </h6>
                                )}
                              </div>
                            ))
                          )}
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              {data.view !== "injected_vaccines" && (
                <button
                  onClick={() => setRedirect(true)}
                  type="button"
                  className="btn btn-secondary"
                >
                  Agregar
                </button>
              )}

              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Cerrar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
  return null;
}
