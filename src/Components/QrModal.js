import QRCode from "react-qr-code";
import { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { io } from "socket.io-client";
import { route } from "../Hooks/baseRoute";

import AuthUser from "../Authentication/AuthUser";
export default function QrModal() {
  const socket = io(route);

  const [redirect, setRedirect] = useState(false);
  const [qrValue, setQrValue] = useState("");

  const { authUser } = AuthUser();

  useEffect(() => {
    socket.on("connect", () => {
      console.log(socket, socket.id);
      setQrValue(
        JSON.stringify({
          doctorId: authUser.id,
          socketId: socket.id,
        })
      );
    });
    socket.on("scanned", (babyId) => {
      setRedirect(babyId);
    });
  }, []);

  if (redirect) {
    return <Redirect push to={`/babysection/${authUser.id}/${redirect}`} />;
  }

  return (
    <div
      className="modal fade"
      id="vinculateBaby"
      data-backdrop="false"
      data-keyboard="false"
      tabIndex="1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="staticBackdropLabel">
              Escanee el código
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body mx-auto ">
            <QRCode value={qrValue} />
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
