import React from "react";
import axios from "axios";
import { route } from "../Hooks";

class Clinicas extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clinics: [], error: null, endPoint: props.endPoint };
    this.removeClinic = this.removeClinic.bind(this);
  }
  componentDidMount = async () => {
    try {
      const userString = sessionStorage.getItem("authUser");
      const authUser = JSON.parse(userString);

      //sets state variable with the promise of the endpoint call
      this.setState({
        clinics: authUser.user.clinics,
      });
      this.props.setEntriesAmount(authUser.user.clinics.length);
    } catch (error) {
      this.setState({ error: error });
    }
  };

  componentDidUpdate = async () => {
    if (this.props.endPoint !== this.state.endPoint) {
      await this.setState({ endPoint: this.props.endPoint });
      this.componentDidMount();
    }
  };

  async removeClinic(entry) {
    const userString = sessionStorage.getItem("authUser");
    var authUser = JSON.parse(userString);

    var clinicsInDoctor = this.state.clinics;

    var confirmation = window.confirm(
      "Está seguro de que desea eliminar esta clínica?"
    );
    if (confirmation) {
      const index = authUser.user.clinics.findIndex(
        (clinicsInDoctor) => clinicsInDoctor.id === entry.id
      );

      if (index > -1) {
        clinicsInDoctor.splice(index, 1);
      }
      const response = await axios.put(
        route + "/users/" + authUser.user.id,
        {
          clinics: clinicsInDoctor,
        },
        { headers: { Authorization: `Bearer ${authUser.jwt}` } }
      );

      authUser.user = response.data;

      sessionStorage.setItem("authUser", JSON.stringify(authUser));
      this.setState({ clinics: response.data.clinics });
    }
  }

  render() {
    const { clinics } = this.state;
    return clinics
      .slice(this.props.limitLeft, this.props.limitRight)
      .map((clinic) => (
        <div className="card border-0 bg-light" key={clinic.id}>
          <div className="card-body p-3">
            <div className="row">
              <div className="col">
                <p>Nombre - Direccion - Sector</p>
                <p>
                  {clinic.name} - {clinic.adress} -{clinic.sector}
                </p>
              </div>
              <div className="col-auto"></div>
              <div className="col-auto">
                <img
                  className="rounded mx-auto d-block w-50 "
                  src={clinic.image ? route + clinic.image.url : undefined}
                  alt={clinic.name}
                />
                <button
                  type="button"
                  onClick={() => this.removeClinic(clinic)}
                  className="btn btn-light"
                >
                  <span className="material-icons-outlined">delete</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      ));
  }
}

export default Clinicas;
