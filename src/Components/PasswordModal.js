import React, { useState, useRef, useEffect } from "react";
import { SignIn } from "./LoginForm";
import { useLocation } from "react-router";

export default function PasswordModal({
  setAuthorized,
  history,
  setAppoinmentInitialState,
}) {
  const passwordInput = useRef();
  const [password, setpassword] = useState("");
  const [raiseAlert, setRaiseAlert] = useState("");
  const location = useLocation();

  useEffect(() => {
    passwordInput.current.focus();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const emailStored = JSON.parse(sessionStorage.getItem("authUser")).user
      .email;
    const trySignIn = await SignIn(emailStored, password);
    if (location.pathname.includes("/editappointment") && trySignIn.success) {
      setAppoinmentInitialState();
    } else if (trySignIn.success) {
      setAuthorized(true);
    }
    if (trySignIn.error) {
      setRaiseAlert("badRequest");
    }
  };
  return (
    <div
      className="position-absolute password_block"
      id="passwordModal"
      data-show="true"
      data-backdrop="false"
      data-keyboard="false"
      tabIndex="1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog   modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title " id="staticBackdropLabel">
              Introduzca su contraseña
            </h5>
            <button
              type="button"
              className="close"
              onClick={() => window.history.back()}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {raiseAlert === "badRequest" && (
              <div className="alert alert-danger " role="alert">
                Contraseña incorrecta
              </div>
            )}
            <form onSubmit={handleSubmit} id="CheckPassword">
              <div className="form-group">
                <label>Contraseña</label>
                <input
                  ref={passwordInput}
                  type="password"
                  name="password"
                  className="form-control"
                  value={password}
                  onChange={(event) => setpassword(event.target.value)}
                  id="exampleInputPassword1"
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              onClick={() => {
                sessionStorage.removeItem("editPrescription");
                window.history.back();
              }}
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Cerrar
            </button>
            <input
              type="submit"
              form="CheckPassword"
              className="btn btn-primary"
              value="Enviar"
            />
          </div>
        </div>
      </div>
    </div>
  );
}
