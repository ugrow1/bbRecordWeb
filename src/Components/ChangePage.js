import React from "react";

class ChangePage extends React.Component {
  constructor(props) {
    super(props);

    this.goBack = this.goBack.bind(this);
    this.goForward = this.goForward.bind(this);
    this.state = { left: 0, right: 5, currentPage: 1 };
  }
  componentDidMount = () => {
    this.setState({ finalPage: Math.ceil(this.props.entriesAmount / 5) });
  };

  componentDidUpdate = (oldProps) => {
    if (this.props.entriesAmount !== oldProps.entriesAmount) {
      this.props.setLimitLeft(0);
      this.props.setLimitRight(5);
      this.setState({
        finalPage: Math.ceil(this.props.entriesAmount / 5),
        left: 0,
        right: 5,
        currentPage: 1,
      });
    }
  };
  goBack() {
    if (
      this.state.left > 1 &&
      this.state.right > 5 &&
      this.state.currentPage > 1
    ) {
      this.setState((prevState) => {
        this.props.setLimitLeft(prevState.left - 5);
        this.props.setLimitRight(prevState.right - 5);
        return {
          left: prevState.left - 5,
          right: prevState.right - 5,
          currentPage: (prevState.currentPage -= 1),
        };
      });
    }

    // eslint-disable-next-line no-sequences
    return this.state.left, this.state.right, this.state.currentPage;
  }

  goForward() {
    if (this.state.currentPage < this.state.finalPage) {
      this.setState((prevState) => {
        this.props.setLimitLeft(prevState.left + 5);
        this.props.setLimitRight(prevState.right + 5);
        return {
          left: prevState.left + 5,
          right: prevState.right + 5,
          currentPage: (prevState.currentPage += 1),
        };
      });
    }
    // eslint-disable-next-line no-sequences
    return this.state.left, this.state.right, this.state.currentPage;
  }

  render() {
    return (
      <div className="card-footer bg-white align-items-center justify-content-center">
        <nav>
          <ul className="pagination w-fit-content pagination-table">
            <li className="page-item">
              <button className="page-link" onClick={this.goBack}>
                <span className="material-icons">keyboard_arrow_left</span>
              </button>
            </li>
            <li className="page-item disabled">
              <button className="page-link">
                {this.state.currentPage} /{" "}
                {this.state.finalPage === 0 ? 1 : this.state.finalPage}
              </button>
            </li>
            <li className="page-item">
              <button className="page-link" onClick={this.goForward}>
                <span className="material-icons">keyboard_arrow_right</span>
              </button>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}
export default ChangePage;
