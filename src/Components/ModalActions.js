import { SendNotificationModal, MedicineModal, PrescriptionModal } from ".";
import { useLocation } from "react-router-dom";

export default function ModalActions({
  correspondingEntry,
  mActions,
  actionsClass,
  mIcon,
  target,
}) {
  const location = useLocation();
  const multiple = mActions !== "single";
  //If there are multiple actions For each action, creates an a element with its url on the href
  //props.actions is a json, wich index is the action and the value is the url
  const actionsList = multiple
    ? mActions.map((mAction, index) => (
        <button
          key={index}
          onClick={() =>
            location.pathname === "/prescriptions"
              ? sessionStorage.setItem("editEntry", correspondingEntry)
              : undefined
          }
          className="dropdown-item"
          data-toggle="modal"
          data-target={
            target[index] === "#passwordModal"
              ? "#passwordModal"
              : target[index] + correspondingEntry
          }
        >
          <div>{mAction}</div>
        </button>
      ))
    : null;
  const toggle = multiple ? "dropdown" : false;
  return (
    //genera el campo de acciones con un dropdown en caso de ser múltiples acciones o sin dropdown en caso de ser una sola
    <div id="actions">
      <button
        type="button"
        className={actionsClass}
        data-toggle={toggle}
        aria-haspopup="true"
        aria-expanded="false"
      >
        <span className="material-icons">{mIcon}</span>
      </button>
      {multiple && (
        <div className="dropdown-menu dropdown-menu-right">{actionsList}</div>
      )}

      {location.pathname === "/medicines" && (
        <MedicineModal medicineId={correspondingEntry} />
      )}
      {location.pathname === "/menu" && (
        <SendNotificationModal patientId={correspondingEntry} />
      )}
      {location.pathname === "/appointments" && (
        <SendNotificationModal patientId={correspondingEntry} />
      )}
      {location.pathname === "/prescriptions" && (
        <PrescriptionModal patientId={correspondingEntry} />
      )}
      {location.pathname === "/inbox" && (
        <SendNotificationModal patientId={correspondingEntry} />
      )}
    </div>
  );
}
