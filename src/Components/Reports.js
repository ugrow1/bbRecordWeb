import React from "react";
import axios from "axios";
import { Actions } from ".";
import { route } from "../Hooks";
import moment from "moment";

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.state = { databaseData: [], error: null, endPoint: props.endPoint };
  }
  componentDidMount = async () => {
    try {
      //gets endpoint data
      const response = await axios.get(this.props.endPoint);
      //sets state variable with the promise of the endpoint call
      this.setState({
        databaseData: response.data,
        endPoint: this.props.endPoint,
      });
      this.props.finalPage(response.data.length);
    } catch (error) {
      this.setState({ error: error });
    }
  };

  componentDidUpdate = () => {
    if (this.props.endPoint !== this.state.endPoint) {
      this.componentDidMount();
    }
  };

  render() {
    moment.locale("es");
    if (this.props.endPoint === this.state.endPoint) {
      if (this.props.filter !== "Todas") {
        return this.state.databaseData
          .slice(this.props.limitLeft, this.props.limitRight)
          .map((data) => (
            <div key={data.id} className="card border-0 bg-light">
              <div className="card-body p-3">
                <div className="row">
                  <div className="col">
                    <p>
                      {data.message} - {"             "}
                      {moment(data.date ? data.date : data.created_at)
                        .subtract(4, "hours")
                        .format("YYYY-MM-DD, h:mm:ss a")}
                    </p>
                  </div>
                  <div className="col-auto"></div>
                  <div className="col-auto">
                    <img
                      className="rounded mx-auto d-block w-50"
                      src={
                        data.image
                          ? route + data.image.url
                          : route + data.baby.image
                          ? route + data.baby.image.url
                          : undefined
                      }
                      alt={data.name}
                    />
                    {this.props.filter === "Solicitudes" && (
                      <td>
                        <Actions
                          correspondingEntry={data.baby?.id}
                          actions={this.props.actions}
                        />
                      </td>
                    )}
                  </div>
                </div>
              </div>
            </div>
          ));
      }

      if (this.props.filter === "Todas") {
        return this.state.databaseData
          .slice(this.props.limitLeft, this.props.limitRight)
          .slice(0)
          .reverse()
          .map((data) => (
            <div key={data.id} className="card border-0 bg-light">
              <div className="card-body p-3">
                <div className="row">
                  <div className="col">
                    <p>
                      {data.report_type.name} - {data.creator.first_name}:
                      {data.message}
                    </p>
                  </div>
                  <div className="col-auto"></div>
                  <div className="col-auto">
                    <img
                      className="rounded mx-auto d-block w-50"
                      src={
                        data.image
                          ? route + data.image.url
                          : route + data.baby.image
                          ? route + data.baby.image.url
                          : undefined
                      }
                      alt={data.name}
                    />
                  </div>
                </div>
              </div>
            </div>
          ));
      }
    } else {
      return null;
    }
  }
}

export default Reports;
