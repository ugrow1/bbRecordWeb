function TableHeader(props) {
  const tabletitles = props.tableheaders.map((tableHeader, index) => (
    <th key={index} scope="col">
      {tableHeader}
    </th>
  ));
  return (
    <thead>
      <tr>{tabletitles}</tr>
    </thead>
  );
}
export default TableHeader;
