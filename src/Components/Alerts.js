import React from "react";
import axios from "axios";
class Alerts extends React.Component {
  constructor(props) {
    super(props);
    this.state = { databaseData: [], error: null };
  }
  componentDidMount = async () => {
    try {
      //gets endpoint data
      const response = await axios.get(this.props.endPoint);
      //sets state variable with the promise of the endpoint call
      this.setState({ databaseData: response.data });
    } catch (error) {
      this.setState({ error: error });
    }
  };

  render() {
    return this.state.databaseData.map((data) => (
      <div
        key={data.id}
        className={"alert alert-" + this.props.alertStyle}
        role="alert"
      >
        <p>
          <strong>
            {data.baby.name} - {data.title}
          </strong>
          : {data.message}
        </p>
      </div>
    ));
  }
}
export default Alerts;
