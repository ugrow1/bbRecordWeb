import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import ServiceForms from "../Forms/ServiceForm";

function DeleteServices(props) {
  return (

          <ServiceForms
            method={axios.delete}
            endpoint={route + "/services/" + props.match.params.serviceId}
          />

  );
}

export default DeleteServices;
