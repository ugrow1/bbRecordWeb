import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import VisitForms from "../Upcoming/VisitForms";

function EditVisit(props) {
  return (
    <VisitForms
      method={axios.put}
      endpoint={route + "/visitadores/" + props.match.params.visitadoresId}
      editOrNew="Editar"
      modify
    />
  );
}

export default EditVisit;
