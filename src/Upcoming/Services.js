import React, { useEffect, useState } from "react";
import AuthUser from "../Authentication/AuthUser";
import { TableData, TableHeader, ViewHeader } from "../Components";
import { route } from "../Hooks";

import axios from "axios";

export default function Services() {
  const [offeredServices, setOfferedServices] = useState();
  const [ready, setReady] = useState(false);
  const { token, authUser } = AuthUser();
  const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);

  useEffect(() => {
    async function getData() {
      const data = await axios
        .get(route + "/services", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((response) => setOfferedServices(response.data))
        .catch((error) => console.log(error.response));
      setReady(true);
    }
    getData();
  }, []);
  return ready ? (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Cotización"
        ViewAction="Nuevo servicio"
        Navigate="/nuevoservicio"
      />
      <div className="container-fluid">
        <div className="card w-18">
          <div className="card-body">
            <h5 className="card-title">Servicios</h5>
            {offeredServices.length === 0 ? (
              <p className="card-text">Aún no ofrece ningún servicio</p>
            ) : (
              <table className="table m-0 text-dark">
                <TableHeader
                  tableheaders={["#", "Servicio", "Precio", "Acciones"]}
                />

                <tbody>
                  <TableData
                    endpoint={
                      route + "/services?doctors_in=" + authUser.id
                    }
                    fields={["name", "price"]}
                    actions={["Editar servicio"]}
                    actionsClass={"btn btn-sm btn-outline-secondary"}
                    actionsLinks={["/editservice/"]}
                    actionsIcon="edit"
                  />
                </tbody>
              </table>
            )}
          </div>
        </div>
      </div>
    </div>
  ) : null;
}
