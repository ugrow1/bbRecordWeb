import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import VisitForms from "./VisitForms";

function NewVisit() {
  return (
    <VisitForms
      method={axios.post}
      endpoint={route + "/visitors"}
      editOrNew="Nuevo"
    />
  );
}

export default NewVisit;
