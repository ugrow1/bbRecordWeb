import React from "react";
import moment from "moment";
import { useState } from "react";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
} from "../Components";
import { route } from "../Hooks";

function Agenda() {
  var todayString = moment().format("YYYY-MM-DD") + "T00:00:00.000Z";
  var pastString =
    moment().subtract(5, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var futureString =
    moment().add(5, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";

  const [activeFilter, setActiveFilter] = useState("Todos");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const authUser = JSON.parse(sessionStorage.getItem("authUser"));

  var first, second;
  function Setdayparameter(activeFilter) {
    if (activeFilter === "Pasados") {
      first = pastString;
      second = todayString;
    }
    if (activeFilter === "Futuros") {
      first = todayString;
      second = futureString;
    }
    if (activeFilter === "Todos") {
      first = pastString;
      second = futureString;
    }
    // eslint-disable-next-line no-sequences
    return first, second;
  }
  Setdayparameter(activeFilter);

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Visitadores Medicos"
        ViewAction="Nuevo visitador "
        Navigate="/nuevovisitador"
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={[
                  "Todos",
                  // "Pasados",
                  // "Futuros"
                ]}
                displayFiltered={setActiveFilter}
              />
              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por nombre" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Nombre",
                      "Numero de telefono",
                      "Fecha de primera visita",
                      "Laboratorio",
                      "Acciones",
                    ]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/visitors?name_contains=" +
                        searchName +
                        "&doctor_in=" +
                        authUser.user.id
                      }
                      setEntriesAmount={setEntriesAmount}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      firstComparison={first}
                      secondComparison={second}
                      dateOrTime="date"
                      visit={true}
                      fields={["name", "phone", "date", "clinic.name"]}
                      actions={["Editar visitador"]}
                      actionsLinks={["/editvisitadores/"]}
                      actionsClass={"btn btn-sm btn-outline-secondary"}
                      actionsIcon="edit"
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Agenda;
