import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { NumberInput, SelectRelations, AutoComplete } from "./Inputs";
import { route } from "../Hooks";
import { Link } from "react-router-dom";

class InvoiceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      prescripted: [{ service: "" }],
      services: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePrescriptedChange = this.handlePrescriptedChange.bind(this);
    this.addPrescriptedMedicine = this.addPrescriptedMedicine.bind(this);
    this.removePrescriptedMedicine = this.removePrescriptedMedicine.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handlePrescriptedChange(position, value) {
    var prescriptedTMP = this.state.prescripted;
    prescriptedTMP[position] = { ...prescriptedTMP[position], services: value };
    this.setState((prevState) => ({ prescripted: prescriptedTMP }));
  }

  addPrescriptedMedicine() {
    this.setState((state) => ({
      prescripted: [...state.prescripted, { service: "" }],
    }));
  }

  //REMOVES FIELD FOR ENTERING MEDICINES
  removePrescriptedMedicine(index) {
    var prescriptedTMP = this.state.prescripted;
    prescriptedTMP.splice(index, 1);
    this.setState({ prescripted: prescriptedTMP });
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    this.setState({ authUser: user });

    if (this.props.modify) {
      try {
        const medicineResponse = await axios(route + "/medicines", {
          headers: { Authorization: `Bearer ${user.jwt}` },
        });

        const servicesNames = medicineResponse.data.map(
          (medicine) => medicine.name
        );
        const response = await axios.get(
          route + "/invoices/" + this.props.invoiceId,
          { headers: { Authorization: `Bearer ${user.jwt}` } }
        );

        const data = response.data;

        this.setState({
          prescripted: data.services,
          baby: data.baby.id,
          appointment: data.appointment.id,
          services: data.services.id,
          total: data.total,
          redirect: false,
        });
      } catch (error) {}
    }
  }

  async handleSubmit(event) {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    const { baby, prescripted, appointment, services, total } = this.state;
    try {
      event.preventDefault();

      const response = await this.props.method(
        this.props.endpoint,
        {
          baby: baby,
          appointment: appointment,
          doctor: this.state.authUser.user.id,
          services: services,
          total: total,
        },
        { headers: { Authorization: `Bearer ${user.jwt}` } }
      );
      for (let index = 0; index < prescripted.length; index++) {
        if (!prescripted[index].medicine) {
          this.setState({ raiseAlert: "fieldsAreEmpty" });
          return;
        }
      }
      //sets state variable with the promise of the endpoint call
      this.props.setUser(response.data);
    } catch (error) {
      this.setState({ error: error });
    }
    this.setState({ redirect: true });
  }

  render() {
    const authUser = JSON.parse(sessionStorage.getItem("authUser"));
    if (this.state.redirect) {
      window.location.href = "/facturas";
    }
    const {
      handleChange,
      props,

      state: { services, prescripted },
    } = this;

    var medicineRows = prescripted.map((service, index) => (
      <div className="my-5">
        <div className="form-row ">
          <div className="form-group col-md-10">
            <SelectRelations
              field="Servicio"
              name={index.toString()}
              doctors={true}
              query={"?doctors_in=" + authUser.user.id}
              defaultValue={prescripted[index].medicine}
              handleChange={this.handlePrescriptedChange}
              collection="services"
              label="Seleccione el servicio"
              attributes={["name"]}
            />
          </div>
          {index !== 0 && (
            <div className="form-group col-md-1">
              <button
                onClick={() => this.removePrescriptedMedicine(index)}
                className="btn btn-outline-secondary mt-4"
              >
                <span className="material-icons-outlined">delete</span>
              </button>
            </div>
          )}

          {index + 1 === prescripted.length && (
            <div className="form-group col-md-1">
              <button
                onClick={() => this.addPrescriptedMedicine(prescripted.length)}
                className="btn btn-outline-secondary mt-4"
              >
                <span className="material-icons-outlined">add_circle</span>
              </button>
            </div>
          )}
        </div>
      </div>
    ));
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="content-main position-absolute">
          <div class="container rounded mt-5 mb-5">
            <div class="row">
              <div class="col-12">
                <h1 class="h4 mb-4">{this.props.editOrNew} factura</h1>
                <div class="form-group">
                  <SelectRelations
                    name="baby"
                    doctors={true}
                    query={"?doctors_in=" + authUser.user.id}
                    defaultValue={this.state.baby}
                    handleChange={this.handleChange}
                    collection="babies"
                    label="Seleccione el paciente"
                    attributes={["name"]}
                  />
                  <hr></hr>
                </div>
              </div>
              <div class="col-8">
                <div class="card">
                  <div class="card-body">
                    <SelectRelations
                      field="Cita"
                      name="appointment"
                      query={"?doctor_in=" + authUser.user.id}
                      doctors={false}
                      defaultValue={this.state.appointment}
                      handleChange={this.handleChange}
                      collection="appointments"
                      label="Seleccione el numero de cita"
                      attributes={["id"]}
                    />

                    {medicineRows}
                    <NumberInput
                      field="Precio Total"
                      name="total"
                      defaultValue={this.state.total}
                      number={this.state.total}
                      handleChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>

              <div class="col-4">
                <div class="card">
                  <div class="card-body">
                    <input
                      type="submit"
                      className="btn btn-primary w-100 rounded-pill"
                      value="Guardar"
                    />
                  </div>
                </div>
                <Link
                  type="button"
                  to="/facturas"
                  className="btn btn-dark w-100 rounded-pill"
                >
                  Regresar
                </Link>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
InvoiceForm.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default InvoiceForm;
