import React from "react";
import ServiceForms from "../Forms/ServiceForm";

function EditServices(props) {
  return (
    <ServiceForms
      modify
      serviceId={props.match.params.serviceId}
      editOrNew="Editar"
    />
  );
}

export default EditServices;
