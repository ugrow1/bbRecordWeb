import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import InvoiceForm from "../Forms/InvoiceForm";

function EditInvoice(props) {
  return (
    <InvoiceForm
      method={axios.put}
      endpoint={route + "/invoices/" + props.match.params.invoiceId}
      editOrNew="Editar"
      modify
      invoiceId={props.match.params.invoiceId}
    />
  );
}

export default EditInvoice;
