import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import ServiceForms from "../Forms/ServiceForm";

function NewService() {
  return (
    <ServiceForms
      method={axios.post}
      endpoint={route + "/services"}
      editOrNew="Nuevo"
    />
  );
}

export default NewService;
