import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import InvoiceForm from "../Forms/InvoiceForm";

function NewInvoice() {
  return (
    <InvoiceForm
      method={axios.post}
      endpoint={route + "/invoices"}
      editOrNew="Nueva"
    />
  );
}

export default NewInvoice;
