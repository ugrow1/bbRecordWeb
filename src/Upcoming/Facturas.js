import React from "react";
import { useState } from "react";
import { route } from "../Hooks";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
} from "../Components";

function Facturas() {
  const [activeFilter, setActiveFilter] = useState("Todas");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const authUser = JSON.parse(sessionStorage.getItem("authUser"));
  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Facturas"
        ViewAction="Nueva factura"
        Navigate="/nuevafactura"
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                displayFiltered={setActiveFilter}
                filters={["Todas"]}
              />

              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por pacientes" />

                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Pacientes",
                      "Fecha",
                      "Hora",
                      "Total",
                      "Acciones",
                    ]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/invoices?baby.name_contains=" +
                        searchName +
                        "&doctor_in=" +
                        authUser.user.id
                      }
                      setEntriesAmount={setEntriesAmount}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      fields={["baby.name", "appointment.date", "total"]}
                      actions={["Editar factura"]}
                      actionsClass={"btn btn-sm btn-outline-secondary"}
                      actionsLinks={["/editfactura/"]}
                      actionsIcon="edit"
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                left={setLimitLeft}
                right={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Facturas;
