import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { route } from "../Hooks";
import { Link } from "react-router-dom";
import { TextInput, NumberInput } from "./Inputs";

class ServiceForms extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "", price: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount = async () => {
    const userString = sessionStorage.getItem("authUser");
    const authUser = JSON.parse(userString);
    this.setState({
      authUser: authUser,
      redirect: false,
    });

    if (this.props.modify) {
      const response = await axios.get(
        route + "/services/" + this.props.serviceId,
        { headers: { Authorization: `Bearer ${authUser.jwt}` } }
      );

      this.setState({ name: response.data.name, price: response.data.price });
      console.log(response.data.name);
    }
  };
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  async handleSubmit(event) {
    const userString = sessionStorage.getItem("authUser");
    const authUser = JSON.parse(userString);

    var method = axios.post;
    var id = "";

    if (this.props.modify) {
      method = axios.put;
      id = this.props.serviceId;
    }

    try {
      event.preventDefault();
      const response = await method(
        route + "/services/" + id,
        {
          name: this.state.name,
          price: this.state.price,
          doctors: authUser.user.id,
        },
        { headers: { Authorization: `Bearer ${authUser.jwt}` } }
      );

      //sets state variable with the promise of the endpoint call
      this.props.setUser(response.data);
    } catch (error) {}
    // this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/services";
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="content-main position-absolute">
          <div class="container rounded mt-5 mb-5">
            <div class="row">
              <div class="col-12">
                <h1 class="h4 mb-4">{this.props.editOrNew} servicio</h1>
                <div class="form-group">
                  <TextInput
                    placeholder="Nombre"
                    name="name"
                    text={this.state.name}
                    handleChange={this.handleChange}
                  />
                  <hr></hr>
                </div>
              </div>
              <div class="col-8">
                <div class="card">
                  <div class="card-body">
                    <NumberInput
                      field="Precio"
                      name="price"
                      number={this.state.price}
                      handleChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>

              <div class="col-4">
                <div class="card">
                  <div class="card-body">
                    <input
                      type="submit"
                      className="btn btn-primary w-100 rounded-pill"
                      value="Guardar"
                    />
                  </div>
                </div>
                <Link
                  type="button"
                  to="/services"
                  className="btn btn-dark w-100 rounded-pill"
                >
                  Regresar
                </Link>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
ServiceForms.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default ServiceForms;
