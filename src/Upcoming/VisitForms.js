import axios from "axios";
import React from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import { TextInput, NumberInput, DateInput } from "../Forms/Inputs";

class VisitForms extends React.Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const authUser = JSON.parse(userString);
    this.setState({ authUser: authUser });

    if (this.props.modify) {
      const response = await axios.get(this.props.endpoint, {
        headers: { Authorization: `Bearer ${authUser.jwt}` },
      });
      this.setState({
        redirect: false,
        name: response.data.name,
        phone: response.data.phone,
        clinic: response.data.clinic,
        date: new moment(response.data.date).format("YYYY-MM-DD"),
        doctor: authUser.user ? authUser.user.id : undefined,
      });
    }
  }
  areFieldsEmpty(...fields) {
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field) {
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();

    if (
      this.areFieldsEmpty(
        this.state.name,
        this.state.phone,
        this.state.clinic,
        this.state.date
      )
    ) {
      this.setState({ raiseAlert: "fieldsAreEmpty" });
      return;
    }
    try {
      const response = await this.props.method(this.props.endpoint, {
        name: this.state.name,
        phone: this.state.phone,
        clinic: this.state.clinic,
        date: this.state.date,
        doctor: this.state.authUser.user.id,
      });

      this.props.setUser(response.data);
    } catch (error) {
      console.log(error.data);
      this.setState({ error: error });
    }
    return;
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/visitadoresmedicos";
    }
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div className="content-main position-absolute">
            <div class="container rounded mt-5 mb-5">
              <div class="row">
                <div class="col-12">
                  <h1 class="h4 mb-4">{this.props.editOrNew} visitador</h1>
                  <div class="form-group">
                    <TextInput
                      placeholder="Nombre"
                      name="name"
                      text={this.state.name}
                      handleChange={this.handleChange}
                    />

                    <hr></hr>
                  </div>
                </div>
                <div class="col-8">
                  <div class="card">
                    <div class="card-body">
                      <TextInput
                        field="Laboratorio"
                        name="clinic"
                        text={this.state.clinic}
                        handleChange={this.handleChange}
                      />

                      <NumberInput
                        field="Telefono"
                        name="phone"
                        number={this.state.phone}
                        handleChange={this.handleChange}
                      />

                      <DateInput
                        name="date"
                        date={this.state.date}
                        handleChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>

                <div class="col-4">
                  <div class="card">
                    <div class="card-body">
                      <input
                        type="submit"
                        className="btn btn-primary w-100 rounded-pill"
                        value="Guardar"
                      />
                    </div>
                  </div>
                  <Link
                    type="button"
                    to="/visitadoresmedicos"
                    className="btn btn-dark w-100 rounded-pill"
                  >
                    Regresar
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </form>
        {this.state.raiseAlert === "fieldsAreEmpty" && (
          <div className="alert alert-danger" role="alert">
            Por favor llene todos los campos correctamente
          </div>
        )}
      </>
    );
  }
}
export default VisitForms;
