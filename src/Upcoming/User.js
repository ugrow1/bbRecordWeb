import React from "react";
import { useState } from "react";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
} from "../Components";
import { route } from "../Hooks";
import { AuthUser } from "../Authentication";

function User() {
  const [activeFilter, setActiveFilter] = useState("Buscar por nombres");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const { authUser } = AuthUser();

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Usuarios"
        ViewAction="Registrar usuarios"
        Navigate="/newuser"
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={["Todos"]}
                displayFiltered={setActiveFilter}
              />

              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por nombre" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Usuario",
                      "Rol",
                      "id card",
                      "Número de teléfono",
                      "Email",
                    ]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/users?meta_user.doctor=" +
                        authUser.id +
                        "username_contains=" +
                        searchName
                      }
                      setEntriesAmount={setEntriesAmount}
                      fields={[
                        "username",
                        "role.name",
                        "id_card",
                        "phone",
                        "email",
                      ]}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      dateOrTime="date"
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default User;
