import NewUserForm from "../Forms/Register/NewUserForm";
function NewUser(props) {
  return (
    <div>
      <div className="background_login bg-dark position-absolute w-100 h-100"></div>
      <div className="page_main login_page h-100">
        <div className="container-fluid h-100">
          <div className="row align-items-center p-0 m-0">
            <div className="col-12 p-0 m-0 col-md-auto">
              <div className="card login_box border-0 shadow-lg mx-auto">
                <div className="card-header bg-white p-5">
                  <div className="d-md-flex align-items-center">
                    <figure className="header_logo_login mb-0">
                      <img
                        src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PEA8PDxANDw8PEA0PDw0PDw8PDw0NFREWFxURFRUYHSggGBolGxUTITEhJSkrLi4uFx8zODMsNygtMCsBCgoKDg0OFxAQFzUdHR0rKzctLS0vMy4vKzcuLS0tLS8uLTctKy8rNy03LS8yLS0rLSs3LS0tNysyLSstKysrL//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAAAQIDBwQFBgj/xABGEAACAQMABwQECQkHBQAAAAAAAQIDBBEFBhIhMUFRB2FxgRORocEUIiMyUnKSsdEVQlNiY4KywvAkNENEVKKzM4OTo/H/xAAaAQEBAAMBAQAAAAAAAAAAAAAAAQIDBAUG/8QALxEBAAICAQEGBAQHAAAAAAAAAAECAxEEMQUSEyFBUXGRsdEiMqHwFEJSYYHB4f/aAAwDAQACEQMRAD8A2+ADJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABJAAAAAAAAJAgEkAAAAAAAHjNcO0iy0c5Uo5urpbnQpSWzSl+1qcI+CzLuNPaya96R0g2qtZ0qL/y1vmnTx0k/nT83juQ2N4ab170ZZtwq3MJ1Y7nRofLVE+ktndF/WaPF6S7ZVvVrZt9J3FVJ/Yhn+I0/Hd3IlMm1e/ue1PStTOzO3or9nQTa85uRwJ6+aVlxva37qpw/hijySZZSIPVw110kv8AO3PnPJ2Fr2g6Tjj+0ufdOnRl/Lk8OpsuqjA2po/tPulj0tO3qLqlKnL1ptew9To3tBtKuFUjUovruqQ9a3+w0RCszmW920XY+krO9pVltUqkKi57Mk2vFcV5nINA6N01KElKMnGS4STaa8Gj3+gdd5bo1sVF9Lcprz4P+t42Pfg41jf0q8dqnJS6rhKPijklQAAAAAAAAAAAAAAAAAOJpbSdC0o1Li4mqdGksym/ZFLnJvckuLYGS9vKVCnOtWnClSppynUm8RjHvZo/XvtTr3blb2DqW1tvjKv824uF3fo493zuuOB57XvXa40tV37VK0pyzQtc/wDsqY+dP2LgubfmESZVZFkVRdEAskQWQEolAsgJyWTKl0gJTLxkyhKA5FOtg7C1vnHmdSjJFge+0FrBKEotSaa5p70bN0FrBCulGbSm+EuEZfgz5/t67TPT6F0u4Nb9wG9geb1c08qkVCb6KM3y7n3d56bBltFQTggCASQAAAAAAAABSvWjTjKpOUYQhGU5zk8RhBLLk3ySR839o+us9K3GKblGyoSfoKbyvSS4OvNfSa4Lkn1bPX9tuuDb/JdCW5bMryafznxhQ8OEn+6upqFElVkWiVRZEEouVRYCUXKosBJYqiwFkWRVF0BJZFUWQFkSiCUBeLOXbVWmcSJmgB7DQWlXBrL6G19W9LqrFQk8vHxH1X0WaFtaziz2Gr2mHBre1vXPgBudoq0cXQ2kI3FJTWNpbprpLr4M5rRUY2QWZUoAACAAAOk1z0/HRtlXunhzitijB/4lxLdCPhne+5M7s0V256e9Nd0rKEvk7SO3US4O5qLn9WGPtsSNa3NedSc6lSTnUnKU5zfGdSTzKT8WyiKosjFVkWRVExAsi5QuBZElUWAsWKosgLIsiqJQF0WiURKYGQlFUyyAyRMqMKZkUgM0ZHNtLlxZ12S0ZgbP1J1h9FVipP5OeIT7k+EvJ+82qz5s0febLN66l6V+FWdOTeZ0/kqnVuKWH5x2fPIHdyKFmVMkAAUQACDDeXMKNOpWqPFOlTnVnLpCEXJv1JnyZpS+nc1q1xU+fXqVKsueHOWdnwWceR9D9rukHQ0TcpPErh0rZd8ZyzNfYjM+bqhJVVF0URZEFiyM1nY1a20qUHPZxtYcVhPhxfczk/kO7/QT9cPxMZvWPKZba4Mto3WkzH9olwiyOZ+Rrr9DP1w/EpW0dXpxcp05RisZk3HC345Md+vus8fLEbmk/KXHRZFCyMmldFosoiQMhZMxpkpgZCUUJAyJkqRSKzuXMyq3n9F+wkzEM64r3861mfhAmXUiFbVPov2FlbVPoP2E70e7P+Hzf0T8pFMOZhcyHMyaXJpVces2h2R6WxXqW7e6tT2or9pDf/C5eo1Ipno9TdJ+gvLSpndGtTUvqSezL2SYH0ayCWQZIAgAAABqnt9usUbGjn51WtWa+pBRX/IzSMzbXb7PNxZR+jb1pfaqJfympZklVUWRVEkHptR6nytaP0qal9mWP5j2EjwOq1fYuqfSe1Tf7y3e1I99I8zlxrJv3fXdi373G7vtM/f/AGwzODpSlt0asesJfdk50zFM0VnU7elkrFqzE+rXsd258tzLJnIvbbYqThzhJpfrR4x9mDinsVtFofDcjBbDfU9GRE5KJljJoWTLFCcgXTJTKZLU4OTwv/gWtZtOojcuXYxzLPKP3nYxMNCmopJevqzNE48lu9L6zhcfwMUVnr6ssTInjf03mOJh0jW2KNSXPZaXi9y+8wiNzp2XvFKTafSHRW88rPVtlnIxUtyXgWyd74aVtoyU6rXB4a4Po+Rx2xGQH1lY1/SUqVT9JTpz+1FP3mY6bUqo5aN0dJ8XZWbfj6GJ3JkgAAAAA0n29x/tVo+ttJeqq/xNTVDdHb7bf3Cr3XNNv7El7zS9QkqqixVEogvTm4tSjulFqSfRp5Rs2zulWpwqx4Tinjo+a8nlGsD0mqGlNiTt5vEZvNNvlU5x8/v8Tm5WPvV3Ho9fsflRizdy3S/19HrJmKRlmYpHmw+rs8/rFbYcay+pPw5P3eo6ZxUuPrXE9jXpqUXGSypJpruPJ3NB0ZuEuHGMusTrxW8vg8vlYa7/ABRuJ+rju3fJp+O5hUZ9H5YZnRdG+MtnnW7NwzPluHH9DLoy0beb5JeLRyUWRfGkr2Xi9Zn9/wCGKnadX5L8Tl04pbksEIujXa0z1d+DjYsX5IZImSJjiZImDrhlidTp6vlwpLrty9y+/wBh2NxcRpwc5cFwXOT5JHnNtzlKcvnSefI3Ya7nbze1uTFMfhR1t9P+rNkZBB0vmhlHLAkzHU4PwYH1NqPHGjNGp/6Gy/4YndnG0Zb+ioUKXD0VGjTx02YJe45JkgAAAAA8H20WPpdG+kXG3r0qnhGWab9s4nzxWX3n1pp7RyurW4tn/jUalNPpNx+LLyeH5HynpCg4SlGScZJtST4qSe9eskq4RJUkgsSmVJA9joDTyqpUqzSqrdGb4Vfwl953cjWh3WjdYalNKNTNWC4Nv48V48/P1nHl43n3qfJ9Bwe19RFM/wA/v93rGcG+tY1Y7MvKXOLLWmk6Nb5k1n6Eviz9T4+Rmkc2prPs9rvUy18p3EvLVqU6T2ZrdykuDRMWd/WgpLEkmnyZ1VfRrW+nL92X4m6LRPVyWxWp084/VhRZGN7UfnRa71vRaNSL5+4ySLQzoujHFlnJJZbSXVtJesjbExEM0RVrRpram8L2t9EubOur6Vit1Nbcuu9RXvZ11WpKo9qo8vkuUV0SNlcUz1cHI7Sx441T8U/ozXd060svdBfNj733mNsoRk6YiIjUPnsmS2S02tO5lfaKuRBBWAztdU7B3N/ZUEs+kuaO0v2cZKU/9sZHUs2P2FaK9NpCrctZhaUXh/tqvxY/7VUA322QAZIAAAAAB8/dr2gfg19OpFYp3Xy8d25Tf/UX2t/7yPoE8r2kavfD7Kags1qGatLrJY+PDzXtSEj5imsMqcu+oOL/AK4HEMVSSVJAsCBkCxyqF/WhujVml0b2l6pZOIMkmInqype1J3WdfB2sdOV1x9HLxi0/Yy/5cnzpw8pSR1KZOTDwqezpjn8iP55do9Mz/Rx+0/wMFTSM3+ZS88s4WScl8OvsTzuRPW/0ZZ3FR84x+rFIxuOd7bb6t5GQZRER0c9sl7fmnaUSVBWCxBGRkCckZIbKuQEyZ9G9kOgXZ6MpymsVrt/CqifGMZJKnHygovHWTNK9nerb0nf0qLTdCn8tcvl6GLXxPGTxHzfQ+oEsbluS3JLgkWAABUAAAAAAAAaO7WNUPg9Z3NKPyFdt4XCnV4yh4PivPoaqrU3F4Z9eaU0fSuqM6FZbUKiw+sXykujT3nztrxqlVs60oSWVvcKiW6cOUl+BJV4kEzg08PiVIJySQALAgAWGSBkC2QVGQL5GSuRkC2RkrkZAnIyVyMgTkRi5NRinKUmlGMU3KUnuSS5sq2bk7GNRHmGlbuDXOyoyXX/MSX8Pr6Ae17MdUVouzSqJfC7jZq3Mt2YvHxaKfSKb83I9eSQZIAAAAAABIAgAAdZrBoSjfUXSqrq4VEvjU5dV3dUdmAPnDXLU6taVHGUerjNb4yj1TPE1qLi8NeZ9daT0bRuqbp1oqUXwfOL6p8jT2ufZ3Uo5nTW3T34klw7muRNK1CDs77RU6baw0ddODXFEEAjIAkkqSBIIyAJBGQBIIyAJIMttbzqzjTpQnUqTezCnCLlOcuiS4m6uzzsmVFwutJqM6ixKnZbpU6T+lVfCcv1VuXPPIOi7LezV3ThfX8HG1TUqFtNYd0+U5rlS7vzvDjvdLG5bktyXJIAyQAAAAAAAAAAAAAAAAIkk1hpNPinvTRIA8lrFqLbXScqaVKb5Y+I/wNV6xag17dvMHs8pJZi/M+gSJxTWGk0+KaymND5Nu9DzhnMWjr52kl/WD6k0pqhZXGc0/Ryf51Pd7GeQ0p2V5y6NSnLuknB+8mlaFdKS5Mq0bUv+zK9hnFFzXWDjL2J5OmuNRr2L321x/wCGo/cQeEGD2EtT7z/S3L/7FV+4yUdQtIz+bZXD+tS2F65YA8XgYNl2PZJpOpjap29BdatWLa8oKR6nRXYtQjh3d1OeGm6dvTjSi+5zltNrwSLoaNjFtpJNttJRSy23wSXM97qr2UaQvdmdePwGg8PbrRbrSX6tLj9rBvHQOqmj7D+621KnPGHWadSs/GpLMvLJ3Q0m3ntU9TLHRccW1PNVrE7mpidep3bX5q7o4R6EAoAAAAAAJIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/2Q=="
                        className="photo-adaptable w-50 h-50"
                        alt="page logo"
                      />
                    </figure>
                    <div className="header-title">
                      <h1 className="h5 d-block mb-0 text-dark text-hiddend">
                        <span className="text-uppercase d-block">CarPro</span>
                      </h1>
                    </div>
                  </div>
                </div>
                <div className="card-body px-5">
                  <div className="form_login">
                    <NewUserForm setUser={props.setUser} />
                  </div>
                </div>
                <div className="card-footer px-5 bg-white text-muted small">
                  <div className="">2021 - Creado en 🇩🇴 por uGrow 🚀</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default NewUser;
