import "../css/root.css";
import "../css/media.css";

import AppHeader from "./Header";
import AppNav from "./Nav";
import React from "react";

function App({ logOut }) {
  return (
    <>
      <AppHeader logOut={logOut} key="header" />
      <AppNav key="nav" />
    </>
  );
}

export default App;
