import AuthUser from "../Authentication/AuthUser";
import { Link, useLocation } from "react-router-dom";
import { route } from "../Hooks";
import { QrModal } from "../Components";

function AppHeader({ logOut }) {
  const location = useLocation();
  const { authUser } = AuthUser();
  const clinicsLenght = authUser.clinics.length;
  const clinicsLogos = authUser.clinics.map((clinic) => (
    <figure
      key={clinic.name}
      className="client-brand-logo my-0 mx-auto mx-md-0"
    >
      <img
        src={route + clinic.image?.url}
        className="rounded"
        title={clinic.name}
        alt="Clínicas"
      />
    </figure>
  ));
  if (
    location.pathname.includes("/babysection") ||
    location.pathname.includes("/editbaby")
  ) {
    return null;
  }
  return (
    <header className="bg-white">
      <div className="container-fluid h-100 p-0">
        <div className="row align-items-center justify-content-between h-100 no-gutters">
          <div className="col-auto d-flex align-items-center">
            <div className="brand pl-3 d-none d-md-flex align-items-center">
              <figure className="header-logo mb-0 mr-2">
                <img
                  src={process.env.PUBLIC_URL + "/logo.png"}
                  className="photo-adaptable w-100 h-100"
                  alt=""
                />
              </figure>
              <div className="header-title d-md-block d-none">
                <h1 className="h5 d-block mb-0 text-light">
                  <span className="text-uppercase d-block">Bb record</span>
                </h1>
              </div>
            </div>
          </div>
          <div className="col-auto pl-2 pl-md-4 d-flex align-items-center">
            <button className="p-2 btn btn-light d-block d-md-none btn_menu_left">
              <span className="material-icons">menu</span>
            </button>
          </div>

          <div className="col align-items-center">
            Centros activos: {clinicsLenght} {clinicsLogos}
          </div>
          {!(
            authUser.meta_user.gender &&
            authUser.meta_user.birth_date &&
            authUser.meta_user.city &&
            // authUser.meta_user.country &&
            authUser.meta_user.address
          ) ? (
            <Link to="/profile/edit" type="button">
              <div
                className={
                  "alert alert-primary col align-items-center my-0 mx-10 mx-md-0"
                }
                role="alert"
              >
                <p>Complete los datos de su perfil aquí!</p>
              </div>
            </Link>
          ) : (
            <>
              {!(
                location.pathname.includes("/patients") ||
                location.pathname.includes("/menu")
              ) && (
                <a data-toggle="modal" data-target="#vinculateBaby" href="/">
                  <span className="material-icons-outlined">qr_code_2</span>
                </a>
              )}
              <QrModal />
            </>
          )}

          <div className="col-auto d-flex align-items-center pr-2 pr-md-3">
            <div className="header-user">
              <div className="info-user text-left small text-uppercase d-flex align-items-center">
                <div className="dropdown">
                  <figure
                    className="user_photo mb-0 bg-light rounded-circle mr-md-3 overflow-hidden"
                    type="button"
                    id="dropdownMenu2"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      src={
                        authUser.meta_user.image
                          ? route + authUser.meta_user.image?.url
                          : process.env.PUBLIC_URL + "/744709.png"
                      }
                      alt={authUser.first_name.toUpperCase()}
                      className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                    />
                  </figure>

                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenu2"
                  >
                    <Link
                      to="/profile"
                      className="nav-item-content d-flex justify-content-between"
                    >
                      <button className="dropdown-item" type="button">
                        Perfil
                      </button>
                    </Link>
                    <button
                      className="dropdown-item"
                      type="button"
                      onClick={logOut}
                    >
                      Cerrar sesion
                    </button>
                  </div>
                </div>

                <div className="user_name d-md-block d-none">
                  <span className="d-block text-primary">
                    {authUser.first_name.toUpperCase()}{" "}
                    {authUser.last_name.toUpperCase()}
                  </span>
                  <span className="text-muted">
                    {authUser.meta_doctor.exequatur}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default AppHeader;
