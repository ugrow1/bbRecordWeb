import { Link, useLocation } from "react-router-dom";

function AppNav() {
  const location = useLocation();
  if (
    location.pathname.includes("/babysection") ||
    location.pathname.includes("/editbaby") ||
    location.pathname.includes("/newuser")
  ) {
    return null;
  }
  return (
    <div
      className="nav-main position-absolute"
      id="v-pills-tab"
      role="tablist"
      aria-orientation="vertical"
    >
      <div className="nav_content">
        <ul>
          <li
            className={
              location.pathname === "/menu" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/menu"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">dashboard</span>
              <div className="nav-label d-inline text-left w-100">
                Dashboard
              </div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/agenda" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/agenda"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">
                collections_bookmark
              </span>
              <div className="nav-label d-inline text-left w-100">Agenda</div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/inbox" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/inbox"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">
                notifications_active
              </span>
              <div className="nav-label d-inline text-left w-100">Bandeja</div>
              {/* <span className="badge float-right badge-dark badge-pill">!</span> */}
            </Link>
          </li>
        </ul>
        <label className="text-uppercase font-weight-bold">Consultorio</label>
        <ul>
          <li
            className={
              location.pathname === "/patients" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/patients"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">child_care</span>
              <div className="nav-label d-inline text-left w-100">
                Pacientes
              </div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/appointments"
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link
              to="/appointments"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">
                format_list_bulleted
              </span>
              <div className="nav-label d-inline text-left w-100">Citas</div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/prescriptions"
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link
              to="/prescriptions"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">medication</span>
              <div className="nav-label d-inline text-left w-100">
                Prescripciones
              </div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/balance" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/balance"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">payments</span>
              <div className="nav-label d-inline text-left w-100">Balance</div>
            </Link>
          </li>

          {/* <li
            className={
              location.pathname === "/facturas" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/facturas"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">
                account_balance_wallet
              </span>
              <div className="nav-label d-inline text-left w-100">Facturas</div>
            </Link>
          </li> */}

          {/* <li
            className={
              location.pathname === "/services" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/services"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">widgets</span>
              <div className="nav-label d-inline text-left w-100">
                Cotización
              </div>
            </Link>
          </li> */}
        </ul>

        <ul>
          {/* <li
            className={
              location.pathname === "/user" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/user"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">
                supervised_user_circle
              </span>
              <div className="nav-label d-inline text-left w-100">Usuarios</div>
            </Link>
          </li> */}
          {/* <li
            className={
              location.pathname === "/visitadoresmedicos"
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link
              to="/visitadoresmedicos"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">work_outline</span>
              <div className="nav-label d-inline text-left w-100">
                Visitadores médicos
              </div>
            </Link>
          </li> */}
        </ul>
        <hr />
        <label className="text-uppercase font-weight-bold">Gestionar</label>
        <ul>
          <li
            className={
              location.pathname === "/clinics" ? "nav-item active" : "nav-item"
            }
          >
            <Link
              to="/clinics"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">local_hospital</span>
              <div className="nav-label d-inline text-left w-100">Clinicas</div>
            </Link>
          </li>
          <li
            className={
              location.pathname === "/medicines"
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link
              to="/medicines"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">medication</span>
              <div className="nav-label d-inline text-left w-100">
                Medicamentos
              </div>
            </Link>
          </li>
          {/* <li
            className={
              location.pathname === "/configuracion"
                ? "nav-item active"
                : "nav-item"
            }
          >
            <Link
              to="/configuracion"
              className="nav-item-content d-flex justify-content-between"
            >
              <span className="material-icons-outlined">settings</span>
              <div className="nav-label d-inline text-left w-100">
                Configuración
              </div>
            </Link>
          </li> */}
        </ul>
      </div>
    </div>
  );
}

export default AppNav;
