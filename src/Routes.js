import App from "./Layout/App";
import Menu from "./Landing/Menu";
import * as Views from "./Views";
import AuthUser from "./Authentication/AuthUser";
import NewAppointment from "./New/NewAppointments";
import EditProfile from "./Edit/EditProfile";
import { Redirect, Route, Switch } from "react-router-dom";
import NewPrescription from "./New/NewPrescription";
import NewEvent from "./New/NewEvent";

import EditAppointment from "./Edit/EditAppointments";
import EditEvent from "./Edit/EditEvent";
import EditPrescription from "./Edit/EditPrescription";
import CancelEvent from "./Edit/CancelEvent";
import Clinic from "./Views/Clinics";
import ClinicForm from "./Forms/ClinicForms";
import BBForm from "./Forms/BBForms";
import RequirePasswordToEnter from "./Guards/RequirePasswordToEnter";

import { useQuery } from "./Hooks";

export default function Routes() {
  let query = useQuery();
  const subdomain = window.location.host.split(".")[0];
  const { token, setUser, userLogOut } = AuthUser();
  //Renders unauthenticated routes
  if (!token) {
    //ROUTES THAT MUST BE RENDERED WHEN WE GET A DOMAIN NAME, BECAUSE SUBDOMAINS CANT BE ACCESSED ON MOBILE BY THE IP OF THE LOCAL MACHINE
    // if (subdomain == "account") {
    //   return (
    //     <Switch>
    //       <Route path="/resetpassword">
    //         <Views.ResetPassword code={query.get("code")} />
    //       </Route>
    //       <Route path="*">
    //         <Redirect to="/resetpassword" />
    //       </Route>
    //     </Switch>
    //   );
    // }
    return (
      <Switch>
        <Route path="/login">
          <Views.Login setUser={setUser} />
        </Route>
        <Route path="/registration">
          <Views.Registration setUser={setUser} />
        </Route>
        {/* ROUTES THAT WILL BE ON THE ACCOUNT SUBDOMAIN BUT ARE HERE FOR DEVELOPMENT MATTERS */}
        <Route path="/resetpassword">
          <Views.ResetPassword code={query.get("code")} />
        </Route>
        <Route path="/forgotpassword">
          <Views.ForgotPassword code={query.get("code")} />
        </Route>
        <Route path="/accountconfirmed">
          <Views.AccountConfirmed />
        </Route>
        {/* // */}

        <Route path="*">
          <Redirect to="/login" />
        </Route>
      </Switch>
    );
  }
  //Renders authenticated routes
  return (
    <>
      <App logOut={userLogOut} />

      <RequirePasswordToEnter
        exact
        path="/prescriptions/nuevaprescripcion"
        component={NewPrescription}
      />
      <RequirePasswordToEnter
        exact
        path="/prescriptions/editprescription/:prescriptionId"
        component={EditPrescription}
      />
      {/* <RequirePasswordToEnter
        exact
        path="/appointments/editappointment/:appointmentId"
        component={EditAppointment}
      /> */}

      <Switch>
        <Route path="/menu">
          <Menu />
        </Route>
        <Route path="/agenda" component={Views.Agenda} />
        <Route path="/inbox" component={Views.Inbox} />
        <Route path="/patients" component={Views.Patients} />
        <Route path="/appointments" component={Views.Appointments} />
        <Route
          path="/editbabyfield/:babyId/:fieldName/:insertedData"
          component={BBForm}
        />
        <Route path="/medicines" component={Views.Medicines} />
        <Route path="/prescriptions" component={Views.Prescriptions} />
        <Route
          path="/editappointment/:appointmentId"
          component={EditAppointment}
        />
        <Route path="/balance" component={Views.Balance} />
        <Route path="/nuevacita/:appointmentId" component={NewAppointment} />

        <Route exact path="/profile" component={Views.Profile} />
        <Route exact path="/profile/edit" component={EditProfile} />

        <Route path="/nuevoevento" component={NewEvent} />

        <Route path="/clinics" component={Clinic} />
        <Route path="/editevento/:reportId" component={EditEvent} />

        <Route path="/cancelarevento/:reportId" component={CancelEvent} />

        <Route path="/addclinic" component={ClinicForm} />

        <Route
          path="/babysection/:doctorId/:babyId"
          component={Views.BabySection}
        />
        <Route path="*">
          {/* Commented Routes */}
          {/* <Route path="/services" component={Views.Services} /> */}
          {/* <Route path="/facturas" component={Views.Facturas} /> */}
          {/* <Route path="/user" component={Views.User} /> */}
          {/* <Route path="/visitadoresmedicos" component={Views.MedicVisitors} /> */}
          {/* <Route path="/newuser" component={Views.NewUser} /> */}
          {/* <Route path="/nuevafactura" component={NewInvoice} /> */}
          {/* <Route path="/nuevovisitador" component={NewVisit} /> */}
          {/* <Route path="/nuevoservicio" component={NewService} /> */}
          {/* <Route path="/deleteservice/:serviceId" component={DeleteServices} /> */}
          {/* <Route path="/deleteclinic/:clinicId" component={DeleteClinic} /> */}
          {/* <Route
          path="/deleteprescription/:prescriptionId"
          component={DeletePrescription}
        /> */}
          {/* <Route path="/deleteservice/:serviceId" component={DeleteServices} /> */}
          {/* <Route path="/editservice/:serviceId" component={EditServices} /> */}
          {/* <Route path="/editvisitadores/:visitadoresId" component={EditVisit} /> */}
          {/* <Route path="/editfactura/:invoiceId" component={EditInvoice} /> */}
          <Redirect to="/menu" />
        </Route>
      </Switch>
    </>
  );
}
