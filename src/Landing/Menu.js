import React, { useEffect, useState } from "react";
import moment from "moment";
import AuthUser from "../Authentication/AuthUser";
import {
  QrModal,
  TableData,
  TableHeader,
  Alerts,
  SendNotificationModal,
} from "../Components";
import { useLocation } from "react-router-dom";
import { route } from "../Hooks";
import axios from "axios";

export default function Menu() {
  const { authUser } = AuthUser();
  const [babies, setbabies] = useState();
  var todayString = moment().format("YYYY-MM-DD");

  var tomorrowString = moment().add(1, "d").format("YYYY-MM-DD");
  useEffect(() => {
    const userString = sessionStorage.getItem("authUser");
    var authUser = JSON.parse(userString);
    async function getBabies() {
      const babiesInDoctor = await axios.get(
        route + "/babies?doctors_in=" + authUser.user.id,
        {
          headers: { Authorization: `Bearer ${authUser.jwt}` },
        }
      );
      setbabies(babiesInDoctor.data);
    }
    getBabies();
  }, []);

  return (
    <div className="content-main position-absolute">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12">
            <h2 className="h4 font-weight-normal">
              Bienvenido, {authUser.first_name.toUpperCase()}{" "}
              {authUser.last_name.toUpperCase()}
            </h2>
            <p className="text-dark">{todayString} </p>
            <hr className="my-4" />
            <Alerts
              endPoint={
                route +
                "/reports?report_type.name=Alerta&doctor.id=" +
                authUser.id +
                "&creator.id_ne=" +
                authUser.id
              }
              alertStyle="primary"
            />
          </div>
        </div>
      </div>
      <div className="container" id="tapDash">
        <div className="row justify-content-center">
          <div className="col-12">
            <h3 className="small text-muted text-uppercase">
              Acciones directas
            </h3>
          </div>
          {authUser.clinics.length === 0 || babies?.length === 0 ? (
            <div className="col-6 col-lg col-btn">
              <div className="card mb-4 dash-icon">
                <a href="/appointments">
                  <div className="card-body text-center">
                    <div className="icon_action">
                      <span className="material-icons-outlined h1">
                        bookmark_add
                      </span>
                    </div>
                    <div className="text-dark h6">
                      Programar
                      <br />
                      Cita
                    </div>
                  </div>
                </a>
              </div>
            </div>
          ) : (
            <div className="col-6 col-lg col-btn">
              <div className="card mb-4 dash-icon">
                <a href="/nuevacita/0">
                  <div className="card-body text-center">
                    <div className="icon_action">
                      <span className="material-icons-outlined h1">
                        bookmark_add
                      </span>
                    </div>
                    <div className="text-dark h6">
                      Programar
                      <br />
                      Cita
                    </div>
                  </div>
                </a>
              </div>
            </div>
          )}
          <div className="col-6 col-lg col-btn">
            <div className="card mb-4 dash-icon">
              <a data-toggle="modal" data-target="#vinculateBaby" href="/">
                <div className="card-body text-center">
                  <div className="icon_action">
                    <span className="material-icons-outlined h1">link</span>
                  </div>
                  <div className="text-dark h6">
                    Vincular
                    <br />
                    Bebé
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-6 col-lg col-btn">
            <div className="card mb-4 dash-icon">
              <a href="/prescriptions/nuevaprescripcion">
                <div className="card-body text-center">
                  <div className="icon_action">
                    <span className="material-icons-outlined h1">receipt</span>
                  </div>
                  <div className="text-dark h6">
                    Realizar
                    <br />
                    Prescripción
                  </div>
                </div>
              </a>
            </div>
          </div>

          <div className="col-6 col-lg col-btn">
            <div className="card mb-4 dash-icon">
              <a data-toggle="modal" data-target="#sendNotification" href="/">
                <div className="card-body text-center">
                  <div className="icon_action">
                    <span className="material-icons-outlined h1">
                      announcement
                    </span>
                  </div>
                  <div className="text-dark h6">
                    Enviar
                    <br />
                    Notificación
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <div className="card-body">
                <div className="h5 font-weight-normal mb-4 d-flex">
                  <span className="material-icons-outlined mr-2">class</span>
                  Citas de hoy
                </div>
                <table className="table small m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Paciente",
                      "Padres",
                      "Número de teléfono",
                      "Clínica",
                      "Hora",
                      "Notificar",
                    ]}
                  />
                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/appointments?date_gte=" +
                        todayString +
                        "&date_lte=" +
                        tomorrowString +
                        "&doctor_in=" +
                        authUser.id
                      }
                      fields={fields}
                      actions={[
                        {
                          displayText: "Enviar mensaje",
                          target: "#sendNotification",
                          icon: "notifications",
                        },
                      ]}
                    />
                  </tbody>
                </table>
              </div>
              <div className="card-footer bg-white">
                <a className="btn btn-sm btn-light border" href="/appointments">
                  Ver todas
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <QrModal />
      <SendNotificationModal />
    </div>
  );
}
const fields = [
  { name: "baby.name", type: "string" },
  { name: "creator.first_name", type: "string" },
  { name: "creator.phone", type: "string" },
  { name: "date", type: "dateTime" },
];
