import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import EventForms from "../Forms/EventForms";

function CancelEvent(props) {
  return (
   
          <EventForms
            method={axios.delete}
            endpoint={route + "/reports/" + props.match.params.reportId}
            editOrNew="Cancelar"
            modify reportId={props.match.params.reportId}
            delete
          />

  );
}

export default CancelEvent;
