import React from "react";
import AppointmentsForm from "../Forms/AppointmentsForm";

function EditAppointment({
  match: {
    params: { appointmentId: thisAppointmentId },
  },
}) {
  return (
    <AppointmentsForm
      modify
      appointmentId={thisAppointmentId}
      editOrNew="Editar"
    />
  );
}

export default EditAppointment;
