import React from "react";
import PrescriptionForm from "../Forms/PrescriptionsForm";
import axios from "axios";

function EditPrescription({
  match: {
    params: { prescriptionId },
  },
}) {
  return (
    <PrescriptionForm
      prescriptionId={prescriptionId}
      method={axios.put}
      editOrNew="Editar"
      modify
    />
  );
}

export default EditPrescription;
