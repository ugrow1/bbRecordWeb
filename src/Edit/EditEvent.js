import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import EventForms from "../Forms/EventForms";

function EditEvent(props) {
  return (
    <EventForms
      method={axios.put}
      endpoint={route + "/reports/" + props.match.params.reportId}
      editOrNew="Editar"
      modify
      reportId={props.match.params.reportId}
    />
  );
}

export default EditEvent;
