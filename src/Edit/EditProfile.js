import React from "react";
import AuthUser from "../Authentication/AuthUser";
import { route } from "../Hooks";
import UserForm from "../Forms/UserForm";

function EditProfile() {
  const { authUser } = AuthUser();
  const usersId = authUser.id;
  return (
    <UserForm
      endpoint={route + "/users/" + usersId}
      user={authUser}
      usersId={usersId}
      modify
    />
  );
}

export default EditProfile;
