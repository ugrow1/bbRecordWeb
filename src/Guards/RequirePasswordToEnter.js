import React, { useState } from "react";
import { PasswordModal } from "../Components";
import { Route, useLocation } from "react-router-dom";

const RequirePasswordToEnter = ({ component: Component, path }) => {
  const [authorized, setAuthorized] = useState(false);
  const location = useLocation();
  if (authorized && !location.pathname.includes(path.split(":")[0])) {
    setAuthorized(false);
  }
  return (
    <Route
      exact
      path={path}
      render={(props) =>
        authorized ? (
          <Component {...props} />
        ) : (
          <PasswordModal {...props} setAuthorized={setAuthorized} />
        )
      }
    />
  );
};

export default RequirePasswordToEnter;
