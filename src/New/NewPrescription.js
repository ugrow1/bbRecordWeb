import React from "react";
import { route } from "../Hooks";
import PrescriptionForm from "../Forms/PrescriptionsForm";
import axios from "axios";

function NewPrescription() {
  return (
    <PrescriptionForm method={axios.post} endpoint={route + "/prescriptions"} />
  );
}

export default NewPrescription;
