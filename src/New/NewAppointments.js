import React from "react";
import { route } from "../Hooks";
import AppointmentsForm from "../Forms/AppointmentsForm";
import axios from "axios";

function NewAppointment({
  match: {
    params: { appointmentId: thisBabyId },
  },
})
 { 
  return (
    <AppointmentsForm
      method={axios.post}
      endpoint={route + "/appointments"}
      editOrNew="Nueva"
      event={thisBabyId > 0 ? true : undefined}
      babyId = {thisBabyId}
    />
  );
}

export default NewAppointment;
