import React from "react";
import { route } from "../Hooks";
import axios from "axios";
import EventForms from "../Forms/EventForms";

function NewEvent() {
  return (
    <EventForms
      method={axios.post}
      endpoint={route + "/reports"}
      editOrNew="Nuevo"
    />
  );
}

export default NewEvent;
