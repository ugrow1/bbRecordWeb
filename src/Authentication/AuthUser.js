import { useState } from "react";

export default function AuthUser() {
  const getUser = () => {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    return user?.user;
  };

  
  const getToken = () => {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    return user?.jwt;
  };
  const [authUser, setAuthUser] = useState(getUser);
  const [token, setToken] = useState(getToken());

  const saveUser = (user) => {
    sessionStorage.setItem("authUser", JSON.stringify(user));
    setToken(user.jwt);
    setAuthUser(user);
  };

  const userLogOut = (user) => {
    sessionStorage.removeItem("authUser");
    setToken(undefined);
    setAuthUser(undefined);
  };

  return {
    setUser: saveUser,
    token,
    authUser,
    userLogOut,
  };
}
