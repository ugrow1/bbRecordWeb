let operatingSystem = "Not known";
if (window.navigator.appVersion.indexOf("Win") !== -1) {
  operatingSystem = "Windows";
}
if (window.navigator.appVersion.indexOf("Mac") !== -1) {
  operatingSystem = "MacOS";
}

if (window.navigator.appVersion.indexOf("Android") !== -1) {
  operatingSystem = "Android";
}

const currentOs = operatingSystem;

export default currentOs;
