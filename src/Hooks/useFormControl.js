export default function useFormControl() {
  const areFieldsEmpty = (stateObject, exceptions = []) => {
    for (let [state, value] of Object.entries(stateObject)) {
      if (!exceptions.includes(state)) {
        if (value === "" || value == null) {
          return true;
        }
      }
    }

    return false;
  };

  const arePasswordsEqual = (firstPassword, secondPassword) => {
    if (firstPassword !== secondPassword) {
      return false;
    } else {
      return true;
    }
  };

  return { areFieldsEmpty, arePasswordsEqual };
}
