import React from "react";
import { useState } from "react";
import { route } from "../Hooks";
import { ChangePage, QuickFilter, ViewHeader } from "../Components";
import Clinicas from "../Components/Clinicas";

function Clinic() {
  const [activeFilter, setActiveFilter] = useState("Todas");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const userString = sessionStorage.getItem("authUser");
  const authUser = JSON.parse(userString);

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Clinicas"
        ViewAction="Agregar Clinica"
        Navigate="/addclinic"
      />

      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={["Clinicas"]}
                displayFiltered={setActiveFilter}
              />

              <div className="card-body px-0 tr_padding">
                <table className="table m-0 text-dark">
                  <tbody>
                    <Clinicas
                      endPoint={
                        route + "/clinics?doctors_in=" + authUser.user.id
                      }
                      filter={activeFilter}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      setEntriesAmount={setEntriesAmount}
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Clinic;
