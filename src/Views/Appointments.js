import React from "react";
import { useState } from "react";
import { route } from "../Hooks";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
  QrModal,
} from "../Components";

export default function Appointments() {
  const [activeFilter, setActiveFilter] = useState("Todas");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const authUser = JSON.parse(sessionStorage.getItem("authUser"));
  const babiesAmount = sessionStorage.getItem("babiesAmount");

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Citas"
        ViewAction="Nueva cita"
        Navigate="/nuevacita/0"
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            {babiesAmount > 0 && authUser.user.clinics.length > 0 ? (
              <div className="card">
                <QuickFilter
                  displayFiltered={setActiveFilter}
                  filters={[
                    "Todas",
                    "En proceso",
                    "Abierta",
                    "Para facturar",
                    "Finalizada",
                    "Cancelada",
                  ]}
                />

                <div className="card-body px-0 tr_padding">
                  <FilterBar value={setSearchName} buscador="por pacientes" />

                  <table className="table m-0 text-dark">
                    <TableHeader
                      tableheaders={[
                        "#",
                        "Pacientes",
                        "Padres",
                        "Número de teléfono",
                        "Fecha",
                        "Hora",
                        "Estado",
                        "Acciones",
                      ]}
                    />

                    <tbody>
                      <TableData
                        fields={fields}
                        endpoint={
                          route +
                          "/appointments?baby.name_contains=" +
                          searchName +
                          "&doctor_in=" +
                          authUser.user.id +
                          "&state_contains=" +
                          `${
                            activeFilter === "Todos" || activeFilter === "Todas"
                              ? ""
                              : activeFilter
                          }` +
                          "&_sort=created_at:DESC"
                        }
                        setEntriesAmount={setEntriesAmount}
                        limitLeft={limitLeft}
                        limitRight={limitRight}
                        states={{
                          En_proceso: "primary",
                          Abierta: "info",
                          Para_facturar: "warning",
                          Finalizada: "success",
                          Cancelada: "secondary",
                        }}
                        actions={[
                          {
                            displayText: "Editar cita",
                            link: "/editappointment/",
                            icon: "edit",
                          },
                          {
                            displayText: "Enviar mensaje",
                            target: "#sendNotification",
                            icon: "notifications",
                          },
                        ]}
                      />
                    </tbody>
                  </table>
                </div>
                <ChangePage
                  setLimitLeft={setLimitLeft}
                  setLimitRight={setLimitRight}
                  entriesAmount={entriesAmount}
                />
              </div>
            ) : (
              <div>
                {authUser.user.clinics.length === 0 ? (
                  <a href="/addclinic" type="button" class="btn btn-secondary">
                    Agregar clínica
                  </a>
                ) : (
                  <>
                    <button
                      data-toggle="modal"
                      data-target="#vinculateBaby"
                      type="button"
                      class="btn btn-secondary"
                    >
                      Agregar Bebé
                    </button>
                    <QrModal />
                  </>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

const fields = [
  { name: "baby.name", type: "string" },
  { name: "creator.first_name", type: "string" },
  { name: "creator.phone", type: "string" },
  { name: "date", type: "dateTime" },
];
