import RegisterForm from "../Forms/Register/RegisterForm";
import { AuthenticationLayout } from "../Components";
function Register(props) {
  return (
    <AuthenticationLayout>
      <RegisterForm setUser={props.setUser} />
    </AuthenticationLayout>
  );
}
export default Register;
