import React from "react";
import moment from "moment";
import { useState } from "react";
import { route } from "../Hooks";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  ViewHeader,
  MedicineModal,
} from "../Components";

export default function Medicines({ location }) {
  var todayString = moment().format("YYYY-MM-DD") + "T00:00:00.000Z";
  var nextWeekString =
    moment().startOf("isoWeek").add(1, "w").format("YYYY-MM-DD") +
    "T00:00:00.000Z";
  var monthString =
    moment().set("date", 1).subtract(1, "M").format("YYYY-MM-DD") +
    "T00:00:00.000Z";
  var yearString =
    moment().subtract(1, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var allString =
    moment().subtract(5, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";

  const [activeFilter, setActiveFilter] = useState("Todos");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  var nextDate;
  function Setdayparameter(activeFilter) {
    if (activeFilter === "Esta_semana") {
      nextDate = nextWeekString;
    }
    if (activeFilter === "Este_mes") {
      nextDate = monthString;
    }
    if (activeFilter === "Todos") {
      nextDate = allString;
    }
    if (activeFilter === "Este_año") {
      nextDate = yearString;
    }
    return nextDate;
  }
  Setdayparameter(activeFilter);

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        displayModal
        HeaderText="Medicamentos"
        ViewAction="Nuevo medicamento"
        modalTarget="#medicineData"
        modalToDisplay={<MedicineModal />}
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por medicamento" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={["#", "Medicamento", "Precio", "Acciones"]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route + "/medicines?name_contains=" + searchName
                      }
                      setEntriesAmount={setEntriesAmount}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      fields={fields}
                      actions={[
                        {
                          displayText: "Editar medicamento",
                          target: "#medicineData",
                          icon: "edit",
                        },
                      ]}
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const fields = [
  { name: "name", type: "string" },
  { name: "price", type: "string" },
];
