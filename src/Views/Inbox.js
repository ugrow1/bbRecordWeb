import React from "react";
import { useState } from "react";
import AuthUser from "../Authentication/AuthUser";
import {
  ChangePage,
  Reports,
  FilterBar,
  QuickFilter,
  SendNotificationModal,
  ViewHeader,
} from "../Components";
import { route } from "../Hooks";
import { useLocation } from "react-router-dom";

function Inbox({
  match: {
    params: { babyId: thisBabyId },
  },
}) {
  const [activeFilter, setActiveFilter] = useState("Todas");
  const { authUser } = AuthUser();
  const location = useLocation();
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  return (
    <div className="content-main position-absolute">
      <ViewHeader
        displayModal
        HeaderText="Notificaciones"
        ViewAction="Enviar notificación"
        modalTarget="#sendNotification"
        modalToDisplay={<SendNotificationModal />}
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                displayFiltered={setActiveFilter}
                filters={[
                  "Todas",
                  "Eventos",
                  "Alertas",
                  "Mensajes",
                  "Solicitudes",
                ]}
              />
              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por mensaje" />

                <div className="px-3">
                  <Reports
                    location={location}
                    endPoint={
                      route +
                      "/reports?message_contains=" +
                      searchName +
                      "&doctor_in=" +
                      authUser.id +
                      "&report_type.name_contains=" +
                      `${
                        activeFilter === "Todos" || activeFilter === "Todas"
                          ? ""
                          : activeFilter.substring(0, activeFilter.length - 2)
                      }` +
                      "&_sort=created_at:DESC"
                    }
                    filter={activeFilter}
                    limitLeft={limitLeft}
                    limitRight={limitRight}
                    setEntriesAmount={setEntriesAmount}
                    actions={[
                      {
                        displayText: "Aceptar cita",
                        link: "/nuevacita/",
                        icon: "event_available",
                      },
                      {
                        displayText: "Rechazar cita",
                        target: "#sendNotification",
                        icon: "cancel",
                      },
                    ]}
                  />
                </div>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Inbox;
