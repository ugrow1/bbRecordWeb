import React from "react";
import moment from "moment";
import { useState } from "react";
import AuthUser from "../Authentication/AuthUser";
import { route } from "../Hooks";
import {
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  Statistics,
} from "../Components";

export default function Balance() {
  var todayString =
    moment().add(1, "d").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var thisWeekString =
    moment().startOf("isoWeek").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var lastFifteenDays =
    moment().subtract(15, "d").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var monthString =
    moment().set("date", 1).format("YYYY-MM-DD") + "T00:00:00.000Z";
  var yearString =
    moment().startOf("year").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var allString =
    moment().subtract(100, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";

  const [activeFilter, setActiveFilter] = useState("Todos");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const { authUser } = AuthUser();

  var previousDate;

  switch (activeFilter) {
    case "Esta_semana":
      previousDate = thisWeekString;

      break;

    case "Ultimos_quince_días":
      previousDate = lastFifteenDays;

      break;
    case "Este_mes":
      previousDate = monthString;

      break;
    case "Este_año":
      previousDate = yearString;
      break;

    default:
      previousDate = allString;
      break;
  }

  return (
    <div className="content-main position-absolute">
      <Statistics
        doctor={AuthUser.id}
        today={previousDate}
        nextDate={todayString}
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={[
                  "Todos",
                  "Esta semana",
                  "Ultimos quince días",
                  "Este mes",
                  "Este año",
                ]}
                displayFiltered={setActiveFilter}
              />
              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por pacientes" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={["#", "Paciente", "Fecha", "Hora", "Precio"]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/appointments?state=Finalizada&baby.name_contains=" +
                        searchName +
                        "&doctor_in=" +
                        authUser.id +
                        "&date_gte=" +
                        previousDate +
                        "&date_lte=" +
                        todayString +
                        "&_sort=created_at:DESC"
                      }
                      setEntriesAmount={setEntriesAmount}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      fields={fields}
                    />
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const fields = [
  { name: "baby.name", type: "string" },
  { name: "date", type: "dateTime" },
  { name: "price", type: "price" },
];
