import React from "react";
import { useState } from "react";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
  QrModal,
} from "../Components";
import { route } from "../Hooks";

export default function Patients() {
  const [activeFilter, setActiveFilter] = useState("Buscar por nombres");
  const [searchName, setSearchName] = useState("");
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  const userString = sessionStorage.getItem("authUser");
  const authUser = JSON.parse(userString);

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        displayModal
        HeaderText="Pacientes"
        ViewAction="Presentar código"
        modalTarget="#vinculateBaby"
        modalToDisplay={<QrModal />}
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={["Todos"]}
                displayFiltered={setActiveFilter}
              />

              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por nombre" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Pacientes",
                      "Padres",
                      "Número de teléfono",
                      "Fecha de nacimiento",
                      "Genero",
                      "Acciones",
                    ]}
                  />

                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/babies?name_contains=" +
                        searchName +
                        "&doctors_in=" +
                        authUser.user.id +
                        "&_sort=created_at:DESC"
                      }
                      setEntriesAmount={setEntriesAmount}
                      fields={fields}
                      actions={[
                        {
                          displayText: "Editar bebé",
                          link: `/babysection/${authUser.user.id}/`,
                          icon: "edit",
                        },
                      ]}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
      <QrModal />
    </div>
  );
}
const fields = [
  { name: "name", type: "string" },
  { name: "creator.first_name", type: "string" },
  { name: "creator.phone", type: "string" },
  { name: "birth_date", type: "date" },
  { name: "gender", type: "string" },
];
