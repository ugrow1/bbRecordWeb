import React from "react";
import moment from "moment";
import { useState } from "react";
import { route } from "../Hooks";
import AuthUser from "../Authentication/AuthUser";
import {
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
} from "../Components";

export default function Agenda({ location }) {
  var todayString = moment().format("YYYY-MM-DD") + "T00:00:00.000Z";
  var tomorrowString =
    moment().add(1, "d").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var nextWeekString =
    moment().startOf("isoWeek").add(1, "w").format("YYYY-MM-DD") +
    "T00:00:00.000Z";
  var monthString =
    moment().set("date", 1).add(1, "M").format("YYYY-MM-DD") + "T00:00:00.000Z";
  var yearString = moment().add(2, "y").format("YYYY-MM-DD") + "T00:00:00.000Z";

  const [activeFilter, setActiveFilter] = useState("Todos");
  const [searchName, setSearchName] = useState("");
  const { authUser } = AuthUser();
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  var nextDate;
  function Setdayparameter(activeFilter) {
    if (activeFilter === "Hoy") {
      nextDate = tomorrowString;
    }
    if (activeFilter === "Esta_semana") {
      nextDate = nextWeekString;
    }
    if (activeFilter === "Este_mes") {
      nextDate = monthString;
    }
    if (activeFilter === "Todos") {
      nextDate = yearString;
    }
    return nextDate;
  }
  Setdayparameter(activeFilter);

  return (
    <div className="content-main position-absolute">
      <ViewHeader
        HeaderText="Proximos Eventos"
        ViewAction="Nuevo evento"
        Navigate="/nuevoevento"
      />
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={["Todos", "Hoy", "Esta semana", "Este mes"]}
                displayFiltered={setActiveFilter}
              />
              <div className="card-body px-0 tr_padding">
                <FilterBar
                  value={setSearchName}
                  buscador="por descripción o nombre del evento"
                />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Evento",
                      "Descripción",
                      "Fecha",
                      "Hora",
                      "Acciones",
                    ]}
                  />
                  {console.log(
                    "🚀 ~ file: Agenda.js ~ line 98 ~ Agenda ~ todayString",
                    nextDate
                  )}
                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        `/reports?report_type.id=1&_where[_or][0][message_contains]=${searchName}&_where[_or][1][title_contains]=${searchName}` +
                        "&doctor_in=" +
                        authUser.id +
                        "&date_gte=" +
                        todayString +
                        "&date_lte=" +
                        nextDate +
                        "&_sort=created_at:DESC"
                      }
                      setEntriesAmount={setEntriesAmount}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      fields={fields}
                      actions={[
                        {
                          displayText: "Editar evento",
                          link: "/editevento/",
                          icon: "edit",
                        },
                        {
                          displayText: "Cancelar evento",
                          link: "/cancelarevento/",
                          icon: "cancel",
                        },
                      ]}
                    />
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const fields = [
  { name: "title", type: "string" },
  { name: "message", type: "string" },
  { name: "date", type: "dateTime" },
];
