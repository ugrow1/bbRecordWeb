import React from "react";
import AuthUser from "../Authentication/AuthUser";
import { route } from "../Hooks";
import { Link } from "react-router-dom";

function Profile() {
  window.onload = function () {
    if (!window.location.hash) {
      window.location = window.location + "#loaded";
      window.location.reload();
    }
  };
  const { authUser } = AuthUser();
  return (
    <div className="content-main position-absolute">
      <div class="page-content page-container" id="page-content">
        <div class="padding">
          <div>
            <div class="col-xl-6 col-md-12">
              <div class="card user-card-full">
                <div class="row m-l-0 m-r-0">
                  <div class="col-sm-4 bg-c-lite-green user-profile">
                    <div class="card-block text-center text-white">
                      <div class="m-b-25">
                        {" "}
                        <img
                          src={
                            authUser.meta_user.image
                              ? route + authUser.meta_user.image?.url
                              : process.env.PUBLIC_URL + "/744709.png"
                          }
                          alt={authUser.first_name.toUpperCase()}
                          className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                        />{" "}
                      </div>
                      <h6 class="f-w-600">
                        {authUser.first_name} {authUser.last_name}
                      </h6>
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="card-block">
                      <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Contacto</h6>
                      <div class="row">
                        <div class="col-sm-6">
                          <p class="m-b-10 f-w-600">Email</p>
                          <h6 class="text-muted f-w-400">{authUser.email}</h6>
                        </div>
                        <div class="col-sm-6">
                          <p class="m-b-10 f-w-600">Telefono</p>
                          <h6 class="text-muted f-w-400">{authUser.phone}</h6>
                        </div>
                      </div>
                      <h6 class="m-b-20 m-t-40 p-b-5 b-b-default f-w-600">
                        Identificacion
                      </h6>
                      <div class="row">
                        <div class="col-sm-6">
                          <p class="m-b-10 f-w-600">Exequatur</p>
                          <h6 class="text-muted f-w-400">
                            {authUser.meta_doctor.exequatur}
                          </h6>
                        </div>
                        <div class="col-sm-6">
                          <p class="m-b-10 f-w-600">Numero de identificacion</p>
                          <h6 class="text-muted f-w-400">{authUser.id_card}</h6>
                        </div>
                      </div>

                      <Link to="/profile/edit">Editar</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
