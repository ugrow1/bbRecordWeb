import { JsonDataModal } from "../Components";
import { route } from "../Hooks";
import axios from "axios";
import moment from "moment";
import AuthUser from "../Authentication/AuthUser";
import React from "react";
import { Link } from "react-router-dom";

export default function BabySection({
  match: {
    params: { babyId, doctorId },
  },
}) {
  const { token, authUser } = AuthUser();
  const [babyData, setBabyData] = React.useState();
  const [modalData, setModalData] = React.useState();

  React.useEffect(() => {
    async function getBabyData() {
      const response = await axios.get(route + "/allbabydata/" + babyId, {
        headers: { Authorization: `Bearer ${token}` },
      });

      setBabyData(response.data);
    }
    getBabyData();
  }, []);

  const handleClick = (e) => {
    const values = JSON.parse(e.target.value);
    const dataToDisplay =
      values.data === "cranial-pressures" ? "cranial_pressures" : values.data;
    const label = e.target.name;
    setModalData({
      view:
        values.data === "cranial-pressures" ? "cranial-pressures" : values.data,
      info: babyData[dataToDisplay],
      sufix: values.sufix,
      label: label,
      columns: values.columnNames,
      attributes: values.columnData,
    });
  };

  if (babyData && authUser.id === parseInt(doctorId)) {
    return (
      <div className="content-main position-absolute">
        <div class="page-content page-container" id="page-content">
          <div class="padding">
            <div>
              <div class="col-xl-6 col-md-12">
                <div class="card user-card-full">
                  <div class="row m-l-0 m-r-0">
                    <div class="col-sm-4 bg-c-lite-green user-profile">
                      <div class="card-block text-center text-white">
                        <div class="m-b-25">
                          <img
                            src={
                              babyData.image
                                ? route + babyData.image.url
                                : undefined
                            }
                            alt={babyData.name.toUpperCase()}
                            className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                          />{" "}
                        </div>
                        <h6 class="f-w-600">
                          {babyData.name} {babyData.last_name}
                        </h6>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="card-block">
                        <div class="row">
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Género</p>
                            <h6 class="text-muted f-w-400">
                              {babyData.gender}
                            </h6>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Tipo de sangre</p>
                            <h6 class="text-muted f-w-400">
                              {babyData.blood_type.name}
                            </h6>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Fecha de nacimiento</p>
                            <h6 class="text-muted f-w-400">
                              {new moment(babyData.birth_date).format(
                                "DD/MM/yyyy"
                              )}
                            </h6>
                          </div>
                        </div>
                        <h6 class="m-b-20 p-b-5 b-b-default f-w-600">
                          Historiales
                        </h6>

                        <div class="row">
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Peso</p>
                            <button
                              name="Peso"
                              value={JSON.stringify({
                                data: "weights",
                                sufix: "lb",
                                columnNames: ["Libras", "Fecha"],
                                columnData: ["pounds", "created_at"],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="peso"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Medidas</p>
                            <button
                              name="Medidas"
                              value={JSON.stringify({
                                data: "measures",
                                sufix: "cm",
                                columnNames: ["Tamaño", "Fecha"],
                                columnData: ["size", "created_at"],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="heridas"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Presión craneal</p>
                            <button
                              name="Presión craneal"
                              value={JSON.stringify({
                                data: "cranial-pressures",
                                sufix: "mmHg",
                                columnNames: ["Presión", "Fecha"],
                                columnData: ["pressure", "created_at"],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="heridas"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Lesiones</p>
                            <button
                              name="Lesiones"
                              value={JSON.stringify({
                                data: "injuries",
                                columnNames: ["Lesión", "Detectada", "Curada"],
                                columnData: [
                                  "name",
                                  "detected_at",
                                  "healed_at",
                                ],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="heridas"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Enfermedades</p>
                            <button
                              name="Enfermedades"
                              value={JSON.stringify({
                                data: "suffered_diseases",
                                columnNames: [
                                  "Enfermedad",
                                  "Detectada",
                                  "Curada",
                                ],
                                columnData: [
                                  "disease.name",
                                  "detected_at",
                                  "healed_at",
                                ],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="heridas"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                          <div class="col-sm-6">
                            <p class="m-b-10 f-w-600">Vacunas</p>
                            <button
                              name="Vacunas"
                              value={JSON.stringify({
                                data: "injected_vaccines",
                                columnNames: ["Vacuna"],
                                columnData: ["vaccine.name"],
                              })}
                              onClick={handleClick}
                              type="button"
                              className="btn btn-success"
                              data-toggle="modal"
                              data-whatever="heridas"
                              data-target="#displayData"
                            >
                              Mostrar
                            </button>
                          </div>
                        </div>
                      </div>
                      <div justify-content="center" class="col-sm-11">
                        <Link
                          type="button"
                          to="/patients"
                          className="btn btn-dark w-100 rounded-pill"
                        >
                          Cerrar
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <JsonDataModal data={modalData} babyId={babyId} />
      </div>
    );
  }
  return null;
}
