import React from "react";
import { useState } from "react";
import AuthUser from "../Authentication/AuthUser";
import {
  ChangePage,
  TableData,
  TableHeader,
  FilterBar,
  QuickFilter,
  ViewHeader,
} from "../Components";
import { route } from "../Hooks";
import { useLocation } from "react-router-dom";

export default function Prescriptions() {
  const [activeFilter, setActiveFilter] = useState("Todos");
  const [searchName, setSearchName] = useState("");
  const { authUser } = AuthUser();
  const [limitLeft, setLimitLeft] = useState(0);
  const [limitRight, setLimitRight] = useState(5);
  const [entriesAmount, setEntriesAmount] = useState(0);
  return (
    <div className="content-main position-absolute ">
      <ViewHeader
        Navigate="/prescriptions/nuevaprescripcion"
        HeaderText="Prescripciones"
        ViewAction="Nueva prescripción"
      ></ViewHeader>
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-12"
            aria-labelledby="estadosCuentas"
            id="misEstadosDeCuenta"
            data-parent="#tapDash"
          >
            <div className="card">
              <QuickFilter
                filters={["Todas"]}
                displayFiltered={setActiveFilter}
              />

              <div className="card-body px-0 tr_padding">
                <FilterBar value={setSearchName} buscador="por pacientes" />
                <table className="table m-0 text-dark">
                  <TableHeader
                    tableheaders={[
                      "#",
                      "Pacientes",
                      "Clinica",
                      "Fecha",
                      "Hora",
                      "Acciones",
                    ]}
                  />
                  <tbody>
                    <TableData
                      endpoint={
                        route +
                        "/prescriptions?baby.name_contains=" +
                        searchName +
                        "&doctor_in=" +
                        authUser.id +
                        "&_sort=created_at:DESC"
                      }
                      fields={fields}
                      limitLeft={limitLeft}
                      limitRight={limitRight}
                      setEntriesAmount={setEntriesAmount}
                      actions={[
                        {
                          displayText: "Editar prescripción",
                          link: "/prescriptions/editprescription/",
                          icon: "edit",
                        },
                        {
                          displayText: "Ver prescripcion",
                          target: "#viewPrescription",
                          icon: "preview",
                        },
                      ]}
                    />
                  </tbody>
                </table>
              </div>
              <ChangePage
                setLimitLeft={setLimitLeft}
                setLimitRight={setLimitRight}
                entriesAmount={entriesAmount}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const fields = [
  { name: "baby.name", type: "string" },
  { name: "clinic.name", type: "string" },
  { name: "published_at", type: "dateTime" },
];
