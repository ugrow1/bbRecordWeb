import { LoginForm, AuthenticationLayout } from "../Components";
function Login(props) {
  return (
    <AuthenticationLayout>
      <LoginForm setUser={props.setUser} />
    </AuthenticationLayout>
  );
}
export default Login;
