import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { SelectRelations } from "./Inputs";
import { route } from "../Hooks";
import { Link } from "react-router-dom";

class ClinicForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clinic: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    this.setState({ authUser: user, redirect: false });
    
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    this.state.authUser.user.clinics.push(this.state.clinic);
    var newClinics = this.state.authUser.user.clinics;
    let { authUser } = this.state;

    try {
      const response = await axios.put(
        route + "/users/" + this.state.authUser.user.id,
        {
          clinics: newClinics,
        },
        { headers: { Authorization: `Bearer ${this.state.authUser.jwt}` } }
      );
      sessionStorage.setItem("authUser", JSON.stringify(this.state.authUser));
      var token = authUser.jwt;
      var user = response.data;
      var modifyAuthUser = {
        jwt: token,
        user: user,
      };
      sessionStorage.setItem("authUser", JSON.stringify(modifyAuthUser));
      this.setState({ authUser: modifyAuthUser });
      //sets state variable with the promise of the endpoint call
      this.props.setUser(response.data);
    } catch (error) {}
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/clinics";
    }
    var userId = this.state.authUser?.user.id;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="content-main position-absolute">
          <div class="container rounded mt-5 mb-5">
            <div class="row">
              <div class="col-12">
                <h1 class="h4 mb-4">Agregar Clinica</h1>
                <div class="form-group">
                  {userId && (
                    <SelectRelations
                      name="clinic"
                      noQuery
                      handleChange={this.handleChange}
                      collection="clinics"
                      label="Seleccione una clínica"
                      attributes={["name"]}
                    />
                  )}
                  <hr></hr>
                </div>
              </div>
              <div class="col-8">
                <div class="card"></div>
              </div>

              <div class="col-4">
                <div class="card">
                  <div class="card-body">
                    <input
                      type="submit"
                      className="btn btn-primary w-100 rounded-pill"
                      value="Guardar"
                     
                    />
                  </div>
                </div>
                <Link type="button" to="/clinics" className="btn btn-dark w-100 rounded-pill">Regresar</Link>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
ClinicForm.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default ClinicForm;
