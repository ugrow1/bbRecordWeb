import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import {
  ImageInput,
  TextInput,
  NumberInput,
  SelectRelations,
  DateInput,
} from "./Inputs";
import { route } from "../Hooks";
import moment from "moment";

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id_card: "",
      email: "",
      password: "",
      file: null,
      gender: undefined,
      date: undefined,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.areFieldsEmpty = this.areFieldsEmpty.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }
  //ITERATE EACH FIELD TO CHECK IF ARE EMPTY
  areFieldsEmpty(...fields) {
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field) {
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  handleImage(event) {
    this.setState({
      file: event.target.files[0],
      example: URL.createObjectURL(event.target.files[0]),
    });
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const authUser = JSON.parse(userString);
    this.setState({ authUser: authUser });

    if (this.props.modify) {
      this.setState({
        id_card: authUser.user.id_card,
        email: authUser.user.email,
        firstName: authUser.user.first_name,
        lastName: authUser.user.last_name,
        idCard: authUser.user.id_card,
        phone: authUser.user.phone,
        speciality: authUser.user.meta_doctor.speciality.id,
        address: authUser.user.meta_user.address,
        city: authUser.user.meta_user.city,
        country: authUser.user.meta_user.country,
        gender: authUser.user.meta_user.gender,
        date: authUser.user.meta_user.birth_date
          ? moment(authUser.user.meta_user.birth_date).format("YYYY-MM-DD")
          : undefined,
        show: {
          gender: authUser.user.meta_user.gender ? false : true,
          date: authUser.user.meta_user.birth_date ? false : true,
        },
        redirect: false,
      });
    }
  }
  async handleSubmit(event) {
    event.preventDefault();

    const {
      id_card,
      firstName,
      lastName,
      email,
      address,
      gender,
      phone,
      date,
      country,
      city,
      speciality,
    } = this.state;

    if (
      this.areFieldsEmpty(
        id_card,
        firstName,
        lastName,
        email,
        address,
        city,
        phone,
        email,
        date,
        gender,
        speciality
      )
    ) {
      this.setState({ raiseAlert: "fieldsAreEmpty" });
      return;
    }

    let { authUser } = this.state;

    try {
      // CREATE FORM DATA WITH META USER DATA
      var metaUserData = new FormData();
      if (this.state.file) {
        metaUserData.append(
          "files.image",
          this.state.file,
          "image" + authUser.user.meta_user.id
        );
      }
      metaUserData.append(
        "data",
        JSON.stringify({
          address,
          gender,
          birth_date: date,
          country,
          city,
        })
      );

      //UPDATE META USER
      await axios.put(
        route + "/meta-users/" + authUser.user.meta_user.id,
        metaUserData,
        {
          headers: { Authorization: `Bearer ${authUser.jwt}` },
        }
      );
      //UPDATE USER
      // this.props.setUser(imageResponse.data);
      const response = await axios.put(
        this.props.endpoint,
        {
          email,
          first_name: firstName,
          last_name: lastName,
          id_card,
          phone,
          speciality,
        },
        {
          headers: { Authorization: `Bearer ${authUser.jwt}` },
        }
      );

      //CHANGE USER STORED UN SESSION STORAGE WITH NEW DATA
      var token = authUser.jwt;
      var user = response.data;
      var modifyAuthUser = {
        jwt: token,
        user: user,
      };

      sessionStorage.setItem("authUser", JSON.stringify(modifyAuthUser));
      this.setState({ authUser: modifyAuthUser });

      //sets state variable with the promise of the endpoint call
    } catch (error) {
      this.setState({ error: error });
      console.log(error);
    }

    this.setState({ redirect: true });
  }

  render() {
    const {
      id_card,
      lastName,
      firstName,
      address,
      city,
      gender,
      phone,
      date,
      email,
      show,
      redirect,
      speciality,
    } = this.state;

    if (redirect) {
      // return (
      //   <Switch>
      //     <Redirect push to="/profile" />
      //     <Route path="/profile" />
      //   </Switch>
      // );
      window.location.href = "/profile";
    }
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div className="content-main position-absolute">
            <div class="page-content page-container" id="page-content">
              <div class="padding">
                <div>
                  <div class="col-xl-6 col-md-12">
                    <div class="card user-card-full">
                      <div class="row m-l-0 m-r-0">
                        <div class="col-sm-4 bg-c-lite-green user-profile">
                          <div class="card-block text-center text-white">
                            <div class="m-b-25">
                              {" "}
                              <div class="form-group">
                                {!this.state.example ? (
                                  <img
                                    src={
                                      this.props.user.meta_user.image
                                        ? route +
                                          this.props.user.meta_user.image?.url
                                        : process.env.PUBLIC_URL + "/744709.png"
                                    }
                                    alt={this.props.user.first_name.toUpperCase()}
                                    className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                                  />
                                ) : (
                                  <img
                                    src={this.state.example}
                                    alt="Nueva imagen"
                                    className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                                  />
                                )}
                              </div>
                              <h6 class="f-w-600">
                                <div className="FileUpload">
                                  <ImageInput handleChange={this.handleImage} />
                                </div>
                              </h6>
                              <Link
                                type="button"
                                to="/profile"
                                className="btn btn-dark w-100 rounded-pill"
                              >
                                Regresar
                              </Link>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="card-block">
                            <span class="font-weight-bold">
                              Editar {this.props.user.first_name}
                            </span>
                            <div class="row">
                              <div class="col-sm-10">
                                <TextInput
                                  field="Cédula"
                                  name="id_card"
                                  text={id_card}
                                  handleChange={this.handleChange}
                                />

                                <TextInput
                                  field="Primer Nombre"
                                  name="firstName"
                                  text={firstName}
                                  handleChange={this.handleChange}
                                />

                                <TextInput
                                  field="Apellido"
                                  name="lastName"
                                  text={lastName}
                                  handleChange={this.handleChange}
                                />
                                <div>
                                  <div>
                                    <div>
                                      <div>
                                        <NumberInput
                                          field="Telefono"
                                          name="phone"
                                          number={phone}
                                          handleChange={this.handleChange}
                                        />
                                        <SelectRelations
                                          noQuery
                                          field="Especialidad"
                                          name="speciality"
                                          defaultValue={speciality}
                                          handleChange={this.handleChange}
                                          collection="Specialities"
                                          label="Seleccione la especialidad"
                                          attributes={["name"]}
                                        />
                                        <TextInput
                                          field="Correo Electronico"
                                          name="email"
                                          text={email}
                                          handleChange={this.handleChange}
                                        />
                                        <div>
                                          <TextInput
                                            field="Direccion"
                                            name="address"
                                            text={address}
                                            handleChange={this.handleChange}
                                          />

                                          <SelectRelations
                                            query="?country=1"
                                            field="Ciudad"
                                            name="city"
                                            defaultValue={city}
                                            handleChange={this.handleChange}
                                            collection="Cities"
                                            label="Seleccione la ciudad"
                                            attributes={["name"]}
                                          />
                                        </div>
                                        {show?.gender && (
                                          <div className="form-group">
                                            <label>Sexo</label>
                                            <select
                                              // disabled={gender ? true : false}
                                              value={gender}
                                              name="gender"
                                              className="form-control"
                                              onChange={this.handleChange}
                                            >
                                              <option value={undefined}>
                                                Seleccione una opcion
                                              </option>
                                              <option value={"Masculino"}>
                                                Masculino
                                              </option>
                                              <option value={"Femenino"}>
                                                Femenino
                                              </option>
                                            </select>
                                          </div>
                                        )}
                                        {show?.date && (
                                          <DateInput
                                            // disabled={date ? true : false}
                                            field="de nacimiento"
                                            name="date"
                                            date={date}
                                            handleChange={this.handleChange}
                                          />
                                        )}
                                      </div>
                                    </div>
                                    <div>
                                      <div></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div>
                            <div class="card">
                              <div class="card-body">
                                <input
                                  type="submit"
                                  className="btn btn-dark w-100 rounded-pill"
                                  value="Guardar"
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        {this.state.raiseAlert === "fieldsAreEmpty" && (
          <div className="alert alert-danger" role="alert">
            Por favor llene todos los campos correctamente
          </div>
        )}
      </>
    );
  }
}
UserForm.propTypes = {
  setUser: PropTypes.func.isRequired,
  setMetaUser: PropTypes.func.isRequired,
};
export default UserForm;
