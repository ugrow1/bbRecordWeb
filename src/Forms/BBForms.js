import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { NumberInput, DateInput, SelectRelations, TextInput } from "./Inputs";
import { route } from "../Hooks";
import moment from "moment";

class BBForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      baby: props.match.params.babyId,
      fieldName: props.match.params.fieldName,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();

    const { fieldName, baby } = this.state;
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);

    const header = { headers: { Authorization: `Bearer ${user.jwt}` } };
    if (fieldName === "suffered_diseases") {
      try {
        await axios.post(
          route + "/suffered-diseases",
          {
            baby: baby,
            started_at: this.state.started_at,
            finished_at: this.state.finished_at,
            disease: this.state.disease,
          },
          header
        );
      } catch (error) {
        console.error(error);
      }
    } else {
      try {
        await axios.post(route + `/${fieldName}`, this.state, header);

        //sets state variable with the promise of the endpoint call
      } catch (error) {}
    }
  }

  render() {
    const fieldName = this.state.fieldName;
    var inputConfig;
    switch (fieldName) {
      case "weights":
        inputConfig = [
          { tag: "Peso", attribute: "pounds" },
          { tag: "Medida", attribute: "date" },
        ];
        break;
      case "measures":
        inputConfig = [
          { tag: "Tamaño", attribute: "size" },
          { tag: "Medida", attribute: "date" },
        ];
        break;
      case "cranial-pressures":
        inputConfig = [
          { tag: "Presión", attribute: "pressure" },
          { tag: "Medida", attribute: "date" },
        ];
        break;
      case "injuries":
        inputConfig = [
          { tag: "Lesión", attribute: "name" },
          { tag: "Detectada", attribute: "detected_at" },
          { tag: "Curada", attribute: "healed_at" },
        ];
        break;
      case "suffered_diseases":
        inputConfig = [
          { tag: "Enfermedad", attribute: "disease" },
          { tag: "Comenzó en", attribute: "started_at" },
          { tag: "Terminó en", attribute: "finished_at" },
        ];
        break;
      default:
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <div class="container rounded mt-5 mb-5">
            <div class="row">
              <div class="col-12">
                <h1 class="h4 mb-4"> Editar record</h1>
                <div class="form-group">
                  {fieldName === "suffered_diseases" ? (
                    <SelectRelations
                      name={inputConfig[0].attribute}
                      label="Seleccione la enfermedad"
                      field={inputConfig[0].tag}
                      attributes={["name"]}
                      collection="diseases"
                      noQuery
                      date={this.state[inputConfig[0].attribute]}
                      handleChange={this.handleChange}
                    />
                  ) : fieldName === "injuries" ? (
                    <TextInput
                      field={inputConfig[0].tag}
                      name={inputConfig[0].attribute}
                      text={this.state[inputConfig[0].attribute]}
                      handleChange={this.handleChange}
                    />
                  ) : (
                    <NumberInput
                      field={inputConfig[0].tag}
                      name={inputConfig[0].attribute}
                      number={this.state[inputConfig[0].attribute]}
                      handleChange={this.handleChange}
                    />
                  )}
                  <hr></hr>
                </div>
              </div>
              {(fieldName === "injuries" ||
                fieldName === "suffered_diseases") && (
                <div class="col-8">
                  <div class="card">
                    <div class="card-body">
                      <DateInput
                        field={inputConfig[1].tag}
                        name={inputConfig[1].attribute}
                        date={this.state[inputConfig[1].attribute]}
                        defaultValue={new moment().format("YYYY-MM-DD")}
                        handleChange={this.handleChange}
                      />
                      <DateInput
                        field={inputConfig[2].tag}
                        name={inputConfig[2].attribute}
                        date={this.state[inputConfig[2].attribute]}
                        defaultValue={new moment().format("YYYY-MM-DD")}
                        handleChange={this.handleChange}
                      />
                    </div>
                  </div>
                </div>
              )}

              <div class="col-4">
                <div class="card">
                  <div class="card-body">
                    <input
                      type="submit"
                      className="btn btn-primary w-100 rounded-pill"
                      value="Guardar"
                      onClick={() => window.history.back()}
                    />
                  </div>
                </div>
                <input
                  type="button"
                  onClick={() => window.history.back()}
                  className="btn btn-dark w-100 rounded-pill"
                  value="Regresar"
                ></input>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
BBForm.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default BBForm;
