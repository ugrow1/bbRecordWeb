import axios from "axios";
import { AuthUser } from "../Authentication";
import { route } from "../Hooks";
import React, { useState, useEffect, Fragment } from "react";

export function TextInput({
  field,
  disabled,
  name,
  text,
  handleChange,
  placeholder,
}) {
  return (
    <div className="form-group">
      <label>{field}</label>
      <input
        disabled={disabled ? true : undefined}
        type="text"
        name={name}
        className="form-control"
        value={text}
        onChange={handleChange}
        id="userFirstName"
        placeholder={placeholder}
      />
    </div>
  );
}

//Selects items stored in the database
export function SelectRelations({
  handleChange,
  name,
  label,
  field,
  attributes,
  collection,
  defaultValue,
  noQuery,
  query,
  disabled,
}) {
  const { authUser, token } = AuthUser();
  const [data, setData] = useState();
  useEffect(() => {
    async function getData() {
      if (!query && !noQuery) {
        query = "?doctor_in=" + authUser.id;
      }

      if (noQuery) {
        query = "";
      }
      const dataRequest = await axios.get(
        route + "/" + collection + query,
        token && {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      setData(dataRequest.data);
    }

    getData();
  }, []);
  return (
    <div className="form-group">
      {field && <label>{field}</label>}
      <select
        className="custom-select"
        onChange={handleChange}
        value={defaultValue && defaultValue}
        name={name}
        aria-label="Example select with button addon"
        disabled={disabled ? true : undefined}
      >
        <option>{label}</option>

        {data &&
          data.map((entry) => (
            <option key={entry.id} value={entry.id}>
              {attributes.map((attribute) => entry[attribute])}
            </option>
          ))}
      </select>
    </div>
  );
}

export function NumberInput({
  name,
  number,
  handleChange,
  field,
  defaultValue,
  disabled,
}) {
  return (
    <div className="form-group">
      <label>{field}</label>
      <input
        disabled={disabled ? true : undefined}
        type="number"
        name={name}
        defaultValue={defaultValue}
        className="form-control"
        value={number}
        onChange={handleChange}
        id="userId"
      />
    </div>
  );
}

export function DateInput({
  disabled,
  field,
  withTime,
  name,
  date,
  handleChange,
  defaultValue,
}) {
  return (
    <div className="form-group">
      <label>Fecha {field}</label>
      <input
        type={withTime ? "datetime-local" : "date"}
        name={name}
        disabled={disabled ? true : undefined}
        className="form-control"
        value={date}
        defaultValue={defaultValue}
        onChange={handleChange}
        id="exampleInputPassword1"
      />
    </div>
  );
}

export function SelectInput(props) {
  return (
    <div className="form-group">
      <label>Estado</label>
      <select
        value={props.estado}
        name={props.name}
        className="form-control"
        onChange={props.handleChange}
        disabled={props.disabled ? true : undefined}
      >
        <option value={"En_proceso"}>En proceso</option>
        <option value={"Abierta"}>Abierta</option>
        <option value={"Para_facturar"}>Para facturar</option>
        <option value={"Finalizada"}>Finalizada</option>
        <option value={"Cancelada"}>Cancelada</option>
      </select>
    </div>
  );
}

export function ImageInput({ labelText, handleChange, multiple }) {
  return (
    <div className="form-group">
      <label>{labelText}</label>
      <input
        accept="image/png, image/gif, image/jpeg"
        type="file"
        multiple={multiple ? true : undefined}
        className="form-control"
        onChange={handleChange}
      />
    </div>
  );
}

export function AutoComplete({
  suggestions,
  field,
  name,
  returnValue,
  value,
  disabled,
}) {
  const [state, setState] = useState({
    // The active selection's index
    activeSuggestion: 0,
    // The suggestions that match the user's input
    filteredSuggestions: [],
    // Whether or not the suggestion list is shown
    showSuggestions: false,
    // What the user has entered
    userInput: value ? value : "",
  });

  useEffect(() => {
    setState((prevState) => ({ ...prevState, userInput: value }));
  }, [value]);

  const setValue = (value) => {
    returnValue(name, value);
  };

  const onChange = (e) => {
    const userInput = e.currentTarget.value;

    // Filter our suggestions that contain the user's input
    const filteredSuggestions = suggestions.filter(
      (suggestion) =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value,
    });

    setValue(e.currentTarget.value);
  };

  //SETS THE SELECTED SUGGESTION AS THE INPUT
  const onClick = (e) => {
    setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText,
    });
    setValue(e.currentTarget.innerText);
  };

  //TRAVERSE THROUGH SUGGESTIONS
  const onKeyDown = (e) => {
    const { activeSuggestion, filteredSuggestions } = state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      setState((state) => ({
        ...state,
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion],
      }));
      setValue(filteredSuggestions[activeSuggestion]);
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      setState((state) => ({
        ...state,
        activeSuggestion: activeSuggestion - 1,
      }));
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      setState((state) => ({
        ...state,
        activeSuggestion: activeSuggestion + 1,
      }));
    }
  };

  const { activeSuggestion, filteredSuggestions, showSuggestions, userInput } =
    state;

  //GENERATES SUGGESTIONS
  let suggestionsListComponent;

  if (showSuggestions && userInput) {
    if (filteredSuggestions.length) {
      suggestionsListComponent = (
        <ul class="suggestions">
          {filteredSuggestions.map((suggestion, index) => {
            let className;

            // Flag the active suggestion with a class
            if (index === activeSuggestion) {
              className = "suggestion-active";
            }

            return (
              <li className={className} key={suggestion} onClick={onClick}>
                {suggestion}
              </li>
            );
          })}
        </ul>
      );
    } else {
      suggestionsListComponent = null;
    }
  }

  return (
    <Fragment>
      <div>
        <label>{field}</label>
        <input
          autoComplete="off"
          type="text"
          name={name}
          className="form-control"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
          disabled={disabled ? true : undefined}
        />
      </div>
      {suggestionsListComponent}
    </Fragment>
  );
}
