import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import { TextInput, DateInput, ImageInput } from "./Inputs";
import moment from "moment";
import { route } from "../Hooks";

class EventForms extends React.Component {
  constructor(props) {
    super(props);
    this.state = { file: null };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.areFieldsEmpty = this.areFieldsEmpty.bind(this);
  }
  static propTypes = {
    location: PropTypes.object.isRequired,
  };
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleImage(event) {
    this.setState({
      file: event.target.files[0],
      example: URL.createObjectURL(event.target.files[0]),
    });
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    this.setState({ authUser: user });

    if (this.props.modify) {
      const response = await axios.get(
        route + "/reports/" + this.props.reportId,
        { headers: { Authorization: `Bearer ${user.jwt}` } }
      );

      this.setState({
        title: response.data.title,
        message: response.data.message,
        date: new moment(response.data.date)
          .subtract(4, "h")
          .format("YYYY-MM-DDTHH:MM"),
        image: response.data.image?.url,
        redirect: false,
      });
    }
  }
  //ITERATE EACH FIELD TO CHECK IF ARE FILLED
  areFieldsEmpty(...fields) {
    console.log(fields);
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field || (typeof field == "string" && field.trim().length === 0)) {
        console.log(field);
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  async handleSubmit(event) {
    event.preventDefault();

    if (
      !this.props.delete &&
      this.areFieldsEmpty(
        this.state.title,
        this.state.message,
        this.state.date,
        this.state.file || this.state.image
      )
    ) {
      this.setState({ raiseAlert: "fieldsAreEmpty" });
      return;
    }
    try {
      const userString = sessionStorage.getItem("authUser");
      const authUser = JSON.parse(userString);

      //APPEND ALL INFO TO THE FORMDATA
      const eventData = new FormData();
      if (this.state.file) {
        eventData.append(
          "files.image",
          this.state.file,
          "image" + this.props.reportId
        );
      }
      let date = this.state.date;
      eventData.append(
        "data",
        JSON.stringify({
          title: this.state.title,
          report_type: "1",
          message: this.state.message,
          date: moment(date),
          creator: authUser.user.id,
          doctor: authUser.user.id,
        })
      );

      const response = await this.props.method(this.props.endpoint, eventData);
      this.props.setUser(response.data);
    } catch (error) {
      this.setState({ error: error });
    }
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/agenda";
    }

    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div className="content-main position-absolute">
            <div class="container rounded mt-5 mb-5">
              <div class="row">
                <div class="col-12">
                  <h1 class="h4 mb-4">{this.props.editOrNew} evento</h1>
                  <div class="form-group">
                    <TextInput
                      disabled={this.props.delete}
                      name="title"
                      text={this.state.title}
                      handleChange={this.handleChange}
                      placeholder="Titulo"
                    />
                    {/* <input
                type="text"
                class="form-control bg-white shadow-sm border-0"
              ></input> */}
                    <hr></hr>
                  </div>
                </div>
                <div class="col-8">
                  <div class="card">
                    <div class="card-body">
                      <TextInput
                        disabled={this.props.delete}
                        field="Descripcion"
                        name="message"
                        text={this.state.message}
                        handleChange={this.handleChange}
                      />

                      <DateInput
                        disabled={this.props.delete}
                        withTime={true}
                        name="date"
                        date={this.state.date}
                        handleChange={this.handleChange}
                      />
                      {/* SHOWS PREVIOUS IMAGE WHEN EDITING */}
                      {this.props.modify && !this.state.example ? (
                        <img
                          src={route + this.state.image}
                          alt={this.state.title}
                          className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                        />
                      ) : (
                        this.state.example && (
                          <img
                            src={this.state.example}
                            alt="Nueva imagen"
                            className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                          />
                        )
                      )}

                      <div className="FileUpload">
                        {!this.props.delete && (
                          <ImageInput
                            labelText="Imagen"
                            handleChange={this.handleImage}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="card">
                    <div class="card-body">
                      {this.props.delete ? (
                        <input
                          type="submit"
                          className="btn btn-danger w-100 rounded-pill"
                          value="Cancelar"
                        />
                      ) : (
                        <input
                          type="submit"
                          className="btn btn-primary w-100 rounded-pill"
                          value="Guardar"
                        />
                      )}
                    </div>
                  </div>

                  <input
                    type="button"
                    onClick={() => this.props.history.goBack()}
                    className="btn btn-dark w-100 rounded-pill"
                    value="Regresar"
                  ></input>
                </div>
              </div>
            </div>
          </div>
        </form>
        {this.state.raiseAlert === "fieldsAreEmpty" && (
          <div className="alert alert-danger" role="alert">
            Por favor llene todos los campos correctamente
          </div>
        )}
      </>
    );
  }
}
EventForms.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default withRouter(EventForms);
