import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {
  SelectRelations,
  TextInput,
  NumberInput,
  DateInput,
  SelectInput,
} from "./Inputs";
import { PasswordModal } from "../Components";
import { route } from "../Hooks";

class AppointmentsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      check: true,
      discount: 0,
      state: "Para_facturar",
      askForPassword: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.areFieldsEmpty = this.areFieldsEmpty.bind(this);
    this.setAppoinmentInitialState = this.setAppoinmentInitialState.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  async componentDidMount() {
    const userString = sessionStorage.getItem("authUser");
    const user = JSON.parse(userString);
    this.setState({ authUser: user });

    if (this.props.modify) {
      const response = await axios(
        route + "/appointments/" + this.props.appointmentId,
        { headers: { Authorization: `Bearer ${user.jwt}` } }
      );
      this.setState({
        appointmentData: response.data,
        baby: response.data.baby.id,
        description: response.data.description,
        clinic: response.data.clinic.id,
        price: response.data.price,
        appointmentInitialState: response.data.state,
        state: response.data.state,
        discount: response.data.insurance_discount,
        parent: response.data.parent.id,
        date: new moment(response.data.date)
          .subtract(4, "h")
          .format("YYYY-MM-DDTHH:mm"),
        check: response.data.insured,
        redirect: false,
      });
    }
    if (this.props.event) {
      this.setState({
        baby: this.props.babyId,
      });
    }
  }

  areFieldsEmpty(...fields) {
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field) {
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  setAppoinmentInitialState() {
    this.setState({
      appointmentInitialState: "Abierta",
      askForPassword: false,
    });
  }

  async handleSubmit(event) {
    event.preventDefault();

    //CHECK IF FIELDS ARE EMPTY
    if (
      this.areFieldsEmpty(
        this.state.baby,
        this.state.clinic,
        this.state.description,
        this.state.price,
        this.state.date
      )
    ) {
      this.setState({ raiseAlert: "fieldsAreEmpty" });
      return;
    }

    var method = axios.post;
    var id = "";

    //SETS THE METHOD IF THE ACTION IS TO UPDATE AN APPOINTMENT
    if (this.props.modify) {
      method = axios.put;
      id = this.props.appointmentId;
    }
    const header = {
      headers: { Authorization: `Bearer ${this.state.authUser.jwt}` },
    };

    try {
      const babyResponse = await axios.get(
        route + "/babies/" + this.state.baby,
        header
      );

      const parent = babyResponse.data.creator;

      const response = await method(
        route + "/appointments/" + id,
        {
          baby:
            this.props.thisBabyId > 0 ? this.props.thisbabyId : this.state.baby,
          clinic: this.state.clinic,
          description: this.state.description,
          parent: parent,
          price: this.state.price,
          insurance_discount: this.state.discount,
          date: this.state.date,
          doctor: this.state.authUser.user.id,
          creator: this.state.authUser.user.id,
          state: this.state.state,
          insured: this.state.check,
        },
        header
      );
      this.props.setUser(response.data);
    } catch (error) {
      this.setState({ error });
    }
    this.setState({ redirect: true });
  }

  render() {
    const authUser = JSON.parse(sessionStorage.getItem("authUser"));
    if (this.state.redirect) {
      window.location.href = "/appointments";
    }
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div id="edit" className="content-main position-absolute">
            <div class="container rounded mt-5 mb-5">
              <div class="row">
                <div class="col-12">
                  <h1 class="h4 mb-4">{this.props.editOrNew} cita</h1>
                  <div class="form-group">
                    <TextInput
                      disabled={
                        this.props.delete ||
                        this.state.appointmentInitialState === "Finalizada"
                      }
                      placeholder="Descripcion"
                      name="description"
                      text={this.state.description}
                      handleChange={this.handleChange}
                    />
                    <hr></hr>
                  </div>
                </div>
                <div class="col-8">
                  <div class="card">
                    <div class="card-body">
                      {this.props.modify ? (
                        <TextInput
                          disabled
                          field="Paciente"
                          name="baby"
                          text={
                            this.state.appointmentData &&
                            this.state.appointmentData.baby.name
                          }
                          handleChange={this.handleChange}
                        />
                      ) : this.props.event ? (
                        <SelectRelations
                          field="Paciente"
                          name="baby"
                          noQuery
                          handleChange={this.handleChange}
                          defaultValue={this.props.babyId && this.props.babyId}
                          collection="babies"
                          label="Seleccione el paciente"
                          attributes={["name"]}
                          disabled
                        />
                      ) : (
                        <SelectRelations
                          field="Paciente"
                          name="baby"
                          query={"?doctors_in=" + authUser.user.id}
                          handleChange={this.handleChange}
                          collection="babies"
                          label="Seleccione el paciente"
                          attributes={["name"]}
                        />
                      )}
                      <SelectRelations
                        field="Clínica"
                        name="clinic"
                        query={"?doctors_in=" + authUser.user.id}
                        defaultValue={this.state.clinic}
                        handleChange={this.handleChange}
                        collection="clinics"
                        label="Seleccione la clínica"
                        attributes={["name"]}
                        disabled={
                          this.props.delete ||
                          this.state.appointmentInitialState === "Finalizada"
                        }
                      />

                      <NumberInput
                        field="Precio"
                        name="price"
                        disabled={
                          this.props.delete ||
                          this.state.appointmentInitialState === "Finalizada"
                        }
                        number={this.state.price}
                        handleChange={this.handleChange}
                      />

                      <NumberInput
                        field="Aplicar descuento"
                        name="discount"
                        disabled={
                          this.props.delete ||
                          this.state.appointmentInitialState === "Finalizada"
                        }
                        number={this.state.discount}
                        handleChange={this.handleChange}
                      />

                      <DateInput
                        withTime={true}
                        name="date"
                        disabled={
                          this.props.delete ||
                          this.state.appointmentInitialState === "Finalizada"
                        }
                        date={this.state.date}
                        handleChange={this.handleChange}
                      />

                      <div className="form-group">
                        <label>Tiene seguro?</label>
                        <select
                          value={this.state.check}
                          name="check"
                          className="form-control"
                          disabled={
                            this.props.delete ||
                            this.state.appointmentInitialState === "Finalizada"
                          }
                          onChange={this.handleChange}
                        >
                          <option value={true}>Si</option>
                          <option value={false}>No</option>
                        </select>
                      </div>
                      <div
                        onClick={() =>
                          this.state.appointmentInitialState === "Finalizada" &&
                          this.setState({
                            askForPassword: true,
                          })
                        }
                      >
                        <SelectInput
                          name="state"
                          estado={this.state.state}
                          handleChange={this.handleChange}
                          disabled={
                            this.state.appointmentInitialState === "Finalizada"
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="card">
                    <div class="card-body">
                      <input
                        type="submit"
                        className="btn btn-primary w-100 rounded-pill"
                        value="Guardar"
                      />
                    </div>
                  </div>
                  <input
                    type="button"
                    onClick={() => window.history.back()}
                    className="btn btn-dark w-100 rounded-pill"
                    value="Regresar"
                  ></input>
                </div>
              </div>
            </div>
          </div>
        </form>
        {this.state.raiseAlert === "fieldsAreEmpty" && (
          <div className="alert alert-danger" role="alert">
            Por favor llene todos los campos correctamente
          </div>
        )}
        {this.state.appointmentInitialState === "Finalizada" && (
          <div className="alert alert-danger" role="alert">
            Toque el boton de estado para modificar la cita
          </div>
        )}
        {this.state.askForPassword && (
          <PasswordModal
            setAppoinmentInitialState={this.setAppoinmentInitialState}
          />
        )}
      </>
    );
  }
}
AppointmentsForm.propTypes = {
  setUser: PropTypes.func.isRequired,
};
export default AppointmentsForm;
