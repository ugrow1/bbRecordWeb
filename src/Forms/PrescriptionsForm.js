import axios from "axios";
import React from "react";
import { AutoComplete, TextInput, SelectRelations, ImageInput } from "./Inputs";
import { route } from "../Hooks";
import moment from "moment";

class PrescriptionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prescripted: [{ medicine: "", dose: "" }],
      medicines: [],
      baby: "",
      clinic: "",
      dose: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.areFieldsEmpty = this.areFieldsEmpty.bind(this);
    this.handleImage = this.handleImage.bind(this);
    this.handlePrescriptedChange = this.handlePrescriptedChange.bind(this);
    this.handlePrescriptedDoseChange =
      this.handlePrescriptedDoseChange.bind(this);
    this.addPrescriptedMedicine = this.addPrescriptedMedicine.bind(this);
    this.removePrescriptedMedicine = this.removePrescriptedMedicine.bind(this);
  }
  //MODIFY STATES ON INPUT TEXT CHANGE
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  //HANDLES MEDICINE INPUT CHANGE
  handlePrescriptedChange(position, value) {
    var prescriptedTMP = this.state.prescripted;
    prescriptedTMP[position] = { ...prescriptedTMP[position], medicine: value };
    this.setState((prevState) => ({ prescripted: prescriptedTMP }));
  }

  //HANDLES MEDICINE DOSE INPUT CHANGE
  handlePrescriptedDoseChange(event) {
    const position = event.target.name;
    const value = event.target.value;
    var prescriptedTMP = this.state.prescripted;
    prescriptedTMP[position] = { ...prescriptedTMP[position], dose: value };
    this.setState({ prescripted: prescriptedTMP });
  }

  //ADDS ANOTHER FIELD FOR ENTERING MEDICINES
  addPrescriptedMedicine() {
    this.setState((state) => ({
      prescripted: [...state.prescripted, { medicine: "", dose: "" }],
    }));
  }

  //REMOVES FIELD FOR ENTERING MEDICINES
  removePrescriptedMedicine(index) {
    var prescriptedTMP = this.state.prescripted;
    prescriptedTMP.splice(index, 1);
    this.setState({ prescripted: prescriptedTMP });
  }

  handleImage(event) {
    this.setState({
      files: event.target.files,
      example: URL.createObjectURL(event.target.files[0]),
    });
  }

  async componentDidMount() {
    const authUser = JSON.parse(sessionStorage.getItem("authUser"));

    try {
      const medicineResponse = await axios(route + "/medicines", {
        headers: { Authorization: `Bearer ${authUser.jwt}` },
      });

      const medicinesNames = medicineResponse.data.map(
        (medicine) => medicine.name
      );

      this.setState({
        authUser: authUser,
        medicines: medicinesNames,
        redirect: false,
      });

      if (this.props.modify) {
        const entryToEdit = this.props.prescriptionId;
        const prescriptionResponse = await axios(
          route + "/prescriptions/" + entryToEdit,
          { headers: { Authorization: `Bearer ${authUser.jwt}` } }
        );

        const data = prescriptionResponse.data;

        this.setState({
          prescripted: data.message,
          baby: data.baby.id,
          clinic: data.clinic.id,
          image: data.image[data.image.length - 1]?.url,
        });
      }
    } catch (error) {}
  }
  //ITERATE EACH FIELD TO CHECK IF ARE FILLED
  areFieldsEmpty(...fields) {
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field) {
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  async handleSubmit(event) {
    const { baby, clinic, prescripted, authUser, files } = this.state;
    const entryToEdit = sessionStorage.getItem("editEntry");
    var prescriptionData = new FormData();

    try {
      event.preventDefault();
      if (this.areFieldsEmpty(baby, clinic)) {
        this.setState({ raiseAlert: "fieldsAreEmpty" });
        return;
      }

      for (let index = 0; index < prescripted.length; index++) {
        if (!prescripted[index].medicine || !prescripted[index].dose) {
          this.setState({ raiseAlert: "fieldsAreEmpty" });
          return;
        }
      }

      prescriptionData = new FormData();

      if (files) {
        Array.from(files).forEach((file) => {
          prescriptionData.append("files.image", file);
        });
      }

      prescriptionData.append(
        "data",
        JSON.stringify({
          baby: baby,
          clinic: clinic,
          message: prescripted,
          doctor: authUser.user.id,
          date: new moment().subtract(4, "h").format("YYYY-MM-DDTHH:mm"),
        })
      );
      var response;
      if (this.props.modify) {
        response = await this.props.method(
          route + "/prescriptions/" + entryToEdit,
          prescriptionData,
          { headers: { Authorization: `Bearer ${authUser.jwt}` } }
        );
      }
      response = await this.props.method(
        route + "/prescriptions",
        prescriptionData,
        { headers: { Authorization: `Bearer ${authUser.jwt}` } }
      );

      //sets state variable with the promise of the endpoint call

      this.props.setUser(response.data);
    } catch (error) {
      console.log(error.response);
    }
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/prescriptions";
    }

    const {
      handleChange,
      props,

      state: { clinic, authUser, medicines, prescripted, baby },
    } = this;

    var medicineRows = prescripted.map((medicine, index) => (
      <div className="my-5">
        <div className="form-row ">
          <div className="form-group col-md-10">
            <AutoComplete
              field="Medicamento"
              name={index.toString()}
              value={prescripted[index].medicine}
              returnValue={this.handlePrescriptedChange}
              suggestions={medicines}
            />
          </div>
          {index !== 0 && (
            <div className="form-group col-md-1">
              <button
                type="button"
                onClick={() => this.removePrescriptedMedicine(index)}
                className="btn btn-outline-secondary mt-4"
              >
                <span className="material-icons-outlined">delete</span>
              </button>
            </div>
          )}

          {index + 1 === prescripted.length && (
            <div className="form-group col-md-1">
              <button
                onClick={() => this.addPrescriptedMedicine(prescripted.length)}
                className="btn btn-outline-secondary mt-4"
              >
                <span className="material-icons-outlined">add_circle</span>
              </button>
            </div>
          )}
        </div>
        <TextInput
          field="Dosis"
          text={prescripted[index].dose}
          name={index.toString()}
          handleChange={this.handlePrescriptedDoseChange}
          placeholder="ej: 1 tableta cada 12 horas"
        />
      </div>
    ));

    if (authUser) {
      return (
        <div id="edit" className="content-main position-absolute ">
          <form onSubmit={this.handleSubmit}>
            <div class="container rounded mt-5 mb-5">
              <div class="row">
                <div class="col-12">
                  <h1 class="h4 mb-4">
                    {props.modify ? "Editar" : "Nueva"} prescripcion
                  </h1>
                  <div class="form-group">
                    <SelectRelations
                      name="baby"
                      defaultValue={baby && baby}
                      query={"?doctors_in=" + authUser.user.id}
                      handleChange={handleChange}
                      collection="babies"
                      label="Seleccione el paciente"
                      attributes={["name"]}
                    />
                    <hr></hr>
                  </div>
                </div>
                <div class="col-8">
                  <div class="card">
                    <div class="card-body">
                      <SelectRelations
                        field="Clínica"
                        name="clinic"
                        query={"?doctors_in=" + authUser.user.id}
                        defaultValue={clinic}
                        handleChange={handleChange}
                        collection="clinics"
                        label="Seleccione la clínica"
                        attributes={["name"]}
                      />
                      {medicineRows}
                      {/* SHOWS PREVIOUS IMAGE WHEN EDITING */}
                      {this.props.modify && !this.state.example ? (
                        <img
                          src={route + this.state.image}
                          alt={this.state.title}
                          className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                        />
                      ) : (
                        this.state.example && (
                          <img
                            src={this.state.example}
                            alt="Nueva imagen"
                            className="w-100 h-100 photo-adaptable rounded-circle bg-light border-0"
                          />
                        )
                      )}

                      <ImageInput
                        multiple
                        labelText="Imágenes"
                        handleChange={this.handleImage}
                      />
                    </div>
                  </div>
                </div>

                <div className="col-4">
                  <div className="card">
                    <div className="card-body">
                      <input
                        type="submit"
                        className="btn btn-primary w-100 rounded-pill"
                        value="Guardar"
                      />
                    </div>
                  </div>
                  <input
                    type="button"
                    onClick={() => window.history.back()}
                    className="btn btn-dark w-100 rounded-pill"
                    value="Regresar"
                  ></input>
                </div>
              </div>
            </div>

            {this.state.raiseAlert === "fieldsAreEmpty" && (
              <div className="alert alert-danger" role="alert">
                Por favor llene todos los campos correctamente
              </div>
            )}
          </form>
        </div>
      );
    }
    return null;
  }
}

export default PrescriptionForm;
