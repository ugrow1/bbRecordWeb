import axios from "axios";
import React, { useEffect, useState } from "react";
import { object, string, ref } from "yup";
import { Formik, Field, Form, useField, ErrorMessage } from "formik";
import { Link } from "react-router-dom";
import { route } from "../../Hooks";

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userCreated: false,
      raiseAlert: "",
      step: 1,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.previousStep = this.previousStep.bind(this);
    this.updateSessionStorage = this.updateSessionStorage.bind(this);
    this.validateFormBeforeContinue =
      this.validateFormBeforeContinue.bind(this);
  }
  async componentDidMount() {
    const stepTMP = sessionStorage.getItem("step");
    if (stepTMP) {
      this.setState({ step: parseInt(stepTMP) });
      return;
    }

    sessionStorage.setItem("step", this.state.step);
  }
  async handleSubmit(values) {
    const { firstName, lastName, email, password, exequatur, speciality } =
      values;

    var metaResponse;
    try {
      //CREATE META USER
      metaResponse = await axios.post(route + "/meta-users", {
        country: 1,
      });

      //CREATE USER

      await axios.post(route + "/auth/local/register/doctor", {
        email,
        password,
        exequatur: exequatur,
        speciality: speciality,
        username: email.replace(/\@.*/g, "").toLowerCase(),
        first_name: firstName,
        last_name: lastName,
        meta_user: metaResponse.data.id,
      });

      this.setState({ userCreated: true });
    } catch (error) {
      await axios.delete(route + "/meta-users/" + metaResponse.data.id);

      if (error.response.status === 500) {
        this.setState({ raiseAlert: "exequaturTaken", loading: false });
        return;
      }
      console.log(error.response);

      switch (error.response.data.data[0].messages[0].message) {
        default:
          this.setState({
            raiseAlert: error.response.data.data[0].messages[0],
            loading: false,
          });
          break;
      }
    }
  }

  async checkIfEmailIsRegistered(email) {
    try {
      const response = await axios.get(route + "/users?email=" + email);

      if (response.data.length > 0) {
        return "Este correo electrónico ya existe";
      }
    } catch (error) {
      console.log(
        "🚀 ~ file: RegisterUser.js ~ line 281 ~ checkIfEmailIsRegistered ~ error",
        error
      );
    }
  }

  async validateFormBeforeContinue(formik) {
    const fieldKeys = Object.keys(initialValues());
    var touch = {};
    fieldKeys.forEach((key) => {
      touch[key] = true;
    });
    formik.setTouched(touch);

    if (Object.keys(formik.errors).length == 0) {
      this.nextStep();
    }
  }

  nextStep() {
    var stateTMP = { ...this.state };

    stateTMP.step = this.state.step + 1;

    this.setState((state) => ({ step: state.step + 1 }));

    sessionStorage.setItem("step", stateTMP.step);
  }

  previousStep(formik) {
    var stateTMP = { ...this.state };
    stateTMP.step = this.state.step - 1;
    this.setState((state) => ({ step: state.step - 1 }));
    sessionStorage.setItem("step", stateTMP.step);
    formik.setTouched({});
  }

  updateSessionStorage(stateTMP) {
    sessionStorage.setItem("register-data", JSON.stringify(stateTMP));
  }

  render() {
    const { userCreated, raiseAlert, speciality, step } = this.state;

    return (
      <>
        {raiseAlert?.message && (
          <div className="alert alert-danger" role="alert">
            {raiseAlert.message}
          </div>
        )}
        <Formik
          validateOnMount={true}
          validateOnChange={false}
          initialValues={initialValues()}
          validationSchema={validationSchema[step - 1]}
          onSubmit={this.handleSubmit}
        >
          {(formik) => {
            const errorKeys = Object.keys(formik.errors);
            if (
              step == 1 &&
              errorKeys.length > 0 &&
              !Object.keys(validationSchema[0].fields).includes(errorKeys[0])
            ) {
              formik.setErrors({});
            }

            this.updateSessionStorage(formik.values);

            if (formik.isSubmitting) {
              return <Loading />;
            }
            if (userCreated) {
              sessionStorage.removeItem("register-data");
              sessionStorage.removeItem("step");
              return <UserHasBeenCreated />;
            }
            return (
              <Form>
                {step == 1 && (
                  <UserFields
                    formik={formik}
                    checkIfEmailIsRegistered={this.checkIfEmailIsRegistered}
                  />
                )}
                {step == 2 && (
                  <DoctorFields formik={formik} speciality={speciality} />
                )}

                <div className="nav nav-pills m-0 mt-4 card-header-pills">
                  <ul className="nav nav-pills m-0 mt-4 card-header-pills justify-content-between w-100">
                    <li className="nav-item align-items-center ">
                      {step == 1 ? (
                        <Link
                          to="/login"
                          onClick={() => {
                            sessionStorage.removeItem("register-data");
                            sessionStorage.removeItem("step");
                          }}
                        >
                          Regresar
                        </Link>
                      ) : (
                        <button
                          type="button"
                          className="btn p-0 m-0"
                          onClick={() => {
                            this.previousStep(formik);
                          }}
                        >
                          Atrás
                        </button>
                      )}
                    </li>
                    <li className="nav-item">
                      {step == 2 ? (
                        <input
                          type="submit"
                          value="Enviar"
                          className="btn btn-dark rounded-pill d-flex"
                        />
                      ) : (
                        <button
                          type="button"
                          onClick={() => {
                            this.validateFormBeforeContinue(formik);
                          }}
                          className="btn btn-dark rounded-pill d-flex"
                        >
                          Continuar
                          <span className="pl-1 material-icons-outlined">
                            arrow_forward
                          </span>
                        </button>
                      )}
                    </li>
                  </ul>
                </div>
              </Form>
            );
          }}
        </Formik>
      </>
    );
  }
}

export default RegisterForm;

function UserFields({ formik, checkIfEmailIsRegistered }) {
  return (
    <>
      <div className="row row-cols-2 ">
        <div className="col pr-1">
          <div className="form-group flt-label">
            <TextInput
              label="Nombre"
              placeholder="Nombre"
              name="firstName"
              className="form-control"
              onChange={(e) => {
                formik.handleChange(e);
              }}
              id="userFirstName"
            />
          </div>
        </div>
        <div className="col pl-1">
          <div className="form-group flt-label">
            <TextInput
              label="Apellido"
              type="text"
              name="lastName"
              placeholder="Apellido"
              className="form-control"
              onChange={(e) => {
                formik.handleChange(e);
              }}
              id="userLastName"
            />
          </div>
        </div>
      </div>

      <div className="form-group flt-label">
        <TextInput
          label="Correo electrónico"
          type="email"
          name="email"
          validate={checkIfEmailIsRegistered}
          className="form-control"
          placeholder="Correo electrónico"
          showErrorMessage
          onChange={(e) => {
            formik.handleChange(e);
          }}
          id="userEmail"
        />
      </div>
      <div className="row row-cols-2">
        <div className="col pr-1">
          <div className="form-group flt-label">
            <TextInput
              label="Contraseña"
              type="password"
              name="password"
              placeholder="Contraseña"
              className="form-control"
              onChange={(e) => {
                formik.handleChange(e);
              }}
            />
          </div>
        </div>
        <div className="col pl-1">
          <div className="form-group flt-label">
            <TextInput
              label="Confirmar contraseña"
              type="password"
              name="confirmPassword"
              placeholder="Confirmar contraseña"
              className="form-control"
              showErrorMessage
              onChange={(e) => {
                formik.handleChange(e);
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
}

function DoctorFields({ formik }) {
  const [specialities, setSpecialities] = useState([]);
  useEffect(() => {
    async function getSpecialities() {
      try {
        const response = await axios.get(`${route}/specialities`);
        setSpecialities(response.data);
      } catch (error) {
        console.log(
          "🚀 ~ file: RegisterForm.js ~ line 319 ~ useEffect ~ error",
          error
        );
      }
    }
    getSpecialities();
  }, []);

  const specialitiesList = specialities.map((speciality) => (
    <option key={speciality.id} value={speciality.id}>
      {speciality.name}
    </option>
  ));
  return (
    <>
      <div className="form-group flt-label">
        <TextInput
          label="Exequatur"
          type="text"
          name="exequatur"
          placeholder="Exequatur"
          showErrorMessage
          className="form-control"
          onChange={(e) => {
            formik.handleChange(e);
          }}
        />
      </div>
      <div className="form-group flt-label">
        <SelectInput className="form-control" name="speciality">
          <option value="">Seleccione su especialidad</option>
          {specialitiesList}
        </SelectInput>
      </div>
    </>
  );
}

function UserHasBeenCreated() {
  return (
    <>
      Se le ha enviado un correo con los pasos a seguir
      <ul className="nav nav-pills m-0 mt-4 card-header-pills">
        <li className="nav-item">
          <Link type="button" to="/user">
            Ingresar
          </Link>
        </li>
      </ul>
    </>
  );
}

function Loading() {
  return (
    <div>
      <div className="spinner-border text-primary inline" role="status">
        <span className="sr-only">Loading...</span>
      </div>
      <p>Creando usuario</p>
    </div>
  );
}

function TextInput({ label, showErrorMessage, ...props }) {
  const [field, meta, helpers] = useField(props);
  const { className, ...inputProps } = props;
  const classAttributes = `${
    meta.touched && meta.error && "is-invalid"
  } ${className}`;
  return (
    <div>
      <Field
        className={classAttributes}
        {...field}
        {...inputProps}
        autoComplete="off"
      />
      <label htmlFor={field.name}>{label}</label>
      {showErrorMessage && (
        <ErrorMessage
          component="div"
          name={field.name}
          className="input-error-message"
        />
      )}
    </div>
  );
}
function SelectInput({ label, showErrorMessage, ...props }) {
  const [field, meta] = useField(props);
  const { className, ...inputProps } = props;
  const classAttributes = `${
    meta.touched && meta.error && "is-invalid"
  } ${className}`;
  return (
    <div>
      <select className={classAttributes} {...field} {...inputProps} />
      {showErrorMessage && (
        <ErrorMessage
          component="div"
          name={field.name}
          className="input-error-message"
        />
      )}
    </div>
  );
}

const validationSchema = [
  object({
    firstName: string().required(),
    lastName: string().required(),
    email: string()
      .email("El correo debe ser un correo válido")
      .required("El correo debe ser un correo válido"),
    password: string().required(),
    confirmPassword: string()
      .oneOf([ref("password"), null], "Las contraseñas deben ser iguales")
      .required("Las contraseñas deben ser iguales"),
  }),
  object({
    exequatur: string()
      .min(4, "Debe tener al menos 4 dígitos")
      .max(7)
      .matches(/\d-\d/g, "Introduzca un exequatur válido, ex: 1234-56"),
    speciality: string().required(),
  }),
];
const initialValues = () => {
  const savedData = JSON.parse(sessionStorage.getItem("register-data"));

  if (savedData) {
    return savedData;
  }
  return {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    exequatur: "",
    speciality: "",
  };
};
