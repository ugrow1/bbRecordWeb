import axios from "axios";
import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { route } from "../../Hooks";
import { SelectRelations } from "../Inputs";

class NewUserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      gender: "",
      redirect: false,
      role: 4,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.areFieldsEmpty = this.areFieldsEmpty.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  //ITERATE EACH FIELD TO CHECK IF ARE FILLED
  areFieldsEmpty(...fields) {
    var isEmpty = false;
    fields.forEach((field) => {
      if (!field) {
        return (isEmpty = true);
      }
    });

    return isEmpty;
  }

  async handleSubmit(event) {
    event.preventDefault();
    const userString = sessionStorage.getItem("authUser");
    const authUser = JSON.parse(userString);
    const {
      idCard,
      firstName,
      lastName,
      email,
      password,
      confirmPassword,
      phone,
      role,
    } = this.state;

    if (
      this.areFieldsEmpty(
        idCard,
        firstName,
        lastName,
        email,
        password,
        confirmPassword,
        phone
      )
    ) {
      this.setState({ raiseAlert: "fieldsAreEmpty" });
      return;
    }

    if (password !== confirmPassword) {
      this.setState({ raiseAlert: "passwordsNotEqual" });
      return;
    }
    var metaResponse;
    try {
      //CREATE META USER
      metaResponse = await axios.post(route + "/meta-users", {
        country: 1,
        doctor: authUser ? authUser.user.id : undefined,
      });

      //CREATE USER

      const userResponse = await axios.post(route + "/auth/local/register", {
        email: email,
        password: password,
        username: (firstName + lastName).toLowerCase(),
        first_name: firstName,
        last_name: lastName,
        id_card: idCard,
        phone: phone,
        meta_user: metaResponse.data.id,
      });

      //SETS THE USER ROLE TO DOCTOR, BECAUSE IT CAN'T BE SET ON REGISTER
      const setRoleResponse = await axios.put(
        route + "/users/" + userResponse.data.user.id,
        {
          role: role,
        },
        { headers: { Authorization: `Bearer ${userResponse.data.jwt}` } }
      );
      //UPDATE USER DATA WITH THE NEW ROLE GIVEN
      userResponse.data.user = setRoleResponse.data;
    } catch (error) {
      console.log(error);

      switch (error.response.data.data[0].messages[0].message) {
        case "Email is already taken.":
          this.setState({ raiseAlert: "emailTaken" });
          break;

        default:
          this.setState({ redirect: true });
          break;
      }

      // await axios.delete(route + "/meta-users/" + metaResponse.data.id);
    }
    this.setState({ redirect: true });
  }

  render() {
    if (this.state.redirect) {
      window.location.href = "/user";
    }
    return (
      <>
        {this.state.raiseAlert === "passwordsNotEqual" && (
          <div className="alert alert-danger" role="alert">
            Las contraseñas no son iguales
          </div>
        )}
        {this.state.raiseAlert === "emailTaken" && (
          <div className="alert alert-danger" role="alert">
            Este correo electrónico ya ha sido registrado
          </div>
        )}
        {this.state.raiseAlert === "fieldsAreEmpty" && (
          <div className="alert alert-danger" role="alert">
            Por favor llene todos los campos correctamente
          </div>
        )}
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Cédula</label>
            <input
              type="text"
              name="idCard"
              className="form-control"
              value={this.state.idCard}
              onChange={this.handleChange}
              id="userId"
            />
          </div>
          <div className="form-group">
            <label>Nombre</label>
            <input
              type="text"
              name="firstName"
              className="form-control"
              value={this.state.firstName}
              onChange={this.handleChange}
              id="userFirstName"
            />
          </div>
          <div className="form-group">
            <label>Apellido</label>
            <input
              type="text"
              name="lastName"
              className="form-control"
              value={this.state.lastName}
              onChange={this.handleChange}
              id="userLastName"
            />
          </div>

          <div className="form-group">
            <label>Rol</label>
            <select
              value={this.state.role}
              name="role"
              className="form-control"
              onChange={this.handleChange}
            >
              <option value={4}>Secretari@</option>
              <option value={5}>Enfermer@</option>
            </select>
          </div>
          <div className="form-group">
            <label>Correo electrónico</label>
            <input
              type="email"
              name="email"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
              id="userEmail"
            />
          </div>
          <div className="form-group">
            <label>Contraseña</label>
            <input
              type="password"
              name="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Confirmar contraseña</label>
            <input
              type="password"
              name="confirmPassword"
              className="form-control"
              value={this.state.confirmPassword}
              onChange={this.handleChange}
            />
          </div>

          <div className="form-group">
            <label>Telefono</label>
            <input
              type="number"
              name="phone"
              className="form-control"
              value={this.state.phone}
              onChange={this.handleChange}
              id="phone"
            />
          </div>

          <ul className="nav nav-pills m-0 mt-4 card-header-pills">
            <li className="nav-item">
              <input
                type="submit"
                className="btn btn-primary rounded-pill px-4"
                value="Registar"
              />
              <Link
                type="button"
                to="/user"
                className="btn btn-dark w-100 rounded-pill"
              >
                Regresar
              </Link>
            </li>

            {/* <li className="nav-item">
            <a className="nav-link" href="#">
              Olvidé mi contraseña
            </a>
          </li> */}
          </ul>
        </form>
      </>
    );
  }
}
NewUserForm.propTypes = {
  setUser: PropTypes.func.isRequired,
  setMetaUser: PropTypes.func.isRequired,
};
export default NewUserForm;
